package jdm;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;
import java.util.Objects;

/**
 * Stimulates A download
 * Keeps data essential for a download
 */
public class Download implements Serializable {
    private final int numberOfSegments;
    private URL url;
    private String fileName;
    private String fileLocation;
    private long size;
    private long downloadedSize;
    private Date startTime;
    private double rate;
    private boolean isDownloading;//flag used to see if a thread is downloading this file

    /**
     * Creates a download
     *
     * @param url          Link of file
     * @param fileName     name of download
     * @param fileLocation location going to be saved
     * @param startTime    time started to download
     */
    Download(URL url, String fileName, String fileLocation, Date startTime, int numberOfSegments) {

        //To avoid Error when creating file
        fileName = fileName.replaceAll("/", "");

        this.url = url;
        this.fileName = fileName;
        this.fileLocation = fileLocation;
        size = 0L;
        this.startTime = startTime;
        this.numberOfSegments = numberOfSegments;
        downloadedSize = 0;
        rate = 0.0;
        isDownloading = false;

    }

    /**
     * Check if file is downloaded
     *
     * @return whether download is finished or not
     */
    boolean isCompleted() {
        return downloadedSize == size && size != 0;
    }

    /**
     * When download is cancelled
     */
    void resetDownload() {
        downloadedSize = 0;
        rate = 0.0;
    }

    /**
     * Sets this download as completed
     */
    void completed() {
        downloadedSize = size;
        rate = 0.0;
    }

    @Override
    public String toString() {
        return fileName;
    }

    /**
     * File name and size and url are compared
     *
     * @param o object going to be compared with
     * @return whether they are equal or not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Download)) return false;
        Download download = (Download) o;
        return Objects.equals(url.toString(), download.url.toString()); /*&&
                Objects.equals(fileName, download.fileName);*/
    }

    @Override
    public int hashCode() {
        return Objects.hash(url.toString());
    }

    //Getters and setters :

    public URL getUrl() {
        return url;
    }

    public String getFileName() {
        return fileName;
    }

    void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public double getProgress() {
        return ((0.0 + downloadedSize) / size) * 100.0;
    }

    public long getSize() {
        return size;
    }

    void setSize(Long size) {
        this.size = size;
    }

    public Date getStartTime() {
        return startTime;
    }

    void downloaded(int size) {
        downloadedSize += size;
    }

    public long getDownloadedSize() {
        return downloadedSize;
    }

    public double getRate() {
        return rate;
    }

    void setRate(double rate) {
        this.rate = rate;
    }

    int getNumberOfSegments() {
        return numberOfSegments;
    }

    boolean isDownloading() {
        return isDownloading;
    }

    void setDownloading(boolean downloading) {
        isDownloading = downloading;
    }

}
