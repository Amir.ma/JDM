package jdm.GUI;

import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.DateTimePicker;
import com.github.lgooddatepicker.components.TimePickerSettings;
import com.github.lgooddatepicker.optionalusertools.PickerUtilities;
import jdm.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Date;

/**
 * Adding a new Download Window
 * You can start a download, add it to queue or just add it to list
 */
class NewDownloadWindow extends JFrame {

    private final JTextField urlField;
    private final JTextField saveLocation;
    private JButton queueButton;
    private QueueManager queueManager;

    /**
     * Initializes different fields
     *
     * @param downloadManager manager for downloading operations
     * @param queueManager    manager for queue stuff
     */
    NewDownloadWindow(DownloadManager downloadManager, QueueManager queueManager, ScheduleManager scheduleManager, Image image) {
        super(JDM.getText("Add New Download"));

        this.queueManager = queueManager;

        //Initializing frame
        setIconImage(image);
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = 0.8;
        constraints.ipady = 3;
        constraints.gridheight = 1;
        getContentPane().setBackground(new Color(232, 253, 255));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        //Setting Input URL
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 0.1;
        constraints.gridwidth = 1;
        JLabel urlLabel = new JLabel();
        try {
            urlLabel.setIcon(new ImageIcon(ImageIO.read(new File("./resources/linkIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load URL Icon !");
        }
        urlLabel.setHorizontalAlignment(JLabel.CENTER);
        add(urlLabel, constraints);

        constraints.gridx = 1;
        constraints.weightx = 20;
        constraints.gridwidth = 5;
        urlField = new JTextField();
        urlField.setPreferredSize(new Dimension(250, 20));
        add(urlField, constraints);

        //Setting Save Location
        constraints.gridy = 1;
        constraints.gridx = 0;
        constraints.weightx = 0.1;
        constraints.gridwidth = 1;
        JLabel fileLabel = new JLabel();
        try {
            fileLabel.setIcon(new ImageIcon(ImageIO.read(new File("./resources/FolderIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't File URL Icon !");
        }
        fileLabel.setHorizontalAlignment(JLabel.CENTER);
        fileLabel.setOpaque(true);
        fileLabel.setToolTipText(JDM.getText("Click to Change location"));
        fileLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                fileChooser.setAcceptAllFileFilterUsed(true);
                fileChooser.setCurrentDirectory(new File(downloadManager.getDefaultSaveFileLocation()));
                if (fileChooser.showDialog(NewDownloadWindow.super.getParent(), JDM.getText("OK")) == JFileChooser.APPROVE_OPTION) {
                    String chosenPath = fileChooser.getSelectedFile().toString();
                    saveLocation.setText(chosenPath);
                    SwingUtilities.invokeLater(() -> {
                        saveLocation.revalidate();
                        saveLocation.repaint();
                    });
                } else {
                    System.out.println("No Selection");
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                fileLabel.setBackground(Color.BLUE);
            }

            @Override
            public void mouseExited(MouseEvent e) {
                fileLabel.setBackground(Color.WHITE);
            }
        });
        add(fileLabel, constraints);

        constraints.weightx = 15;
        constraints.gridwidth = 5;
        constraints.gridx = 1;
        saveLocation = new JTextField(downloadManager.getDefaultSaveFileLocation());
        saveLocation.setPreferredSize(new Dimension(250, 20));
        saveLocation.setEditable(false);
        add(saveLocation, constraints);


        //Setting Operation Buttons
        constraints.weightx = 0.5;
        constraints.gridwidth = 1;
        constraints.gridy = 2;
        constraints.gridx = 0;
        JButton addButton = new JButton(JDM.getText("Add To List"));
        addButton.addActionListener(e -> {
            Download inputDownload;
            try {
                inputDownload = downloadManager.newDownload(new URL(urlField.getText()), saveLocation.getText());
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this, JDM.getText("Invalid URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Invalid URL");
                return;
            }
            if (downloadManager.isForbidden(inputDownload.getUrl().toString())) {
                downloadManager.removeDownload(inputDownload);
                JOptionPane.showMessageDialog(this, JDM.getText("Forbidden URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Forbidden URL");
                return;
            }
            GUI.refreshTable();
            setVisible(false);
        });
        add(addButton, constraints);

        constraints.gridx = 1;
        JButton startButton = new JButton(JDM.getText("Add And Start"));
        startButton.addActionListener(e -> {
            Download inputDownload;
            try {
                inputDownload = downloadManager.newDownload(new URL(urlField.getText()), saveLocation.getText());
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this, JDM.getText("Invalid URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Invalid URL");
                return;
            }
            if (downloadManager.isForbidden(inputDownload.getUrl().toString())) {
                downloadManager.removeDownload(inputDownload);
                JOptionPane.showMessageDialog(this, JDM.getText("Forbidden URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Forbidden URL");
                return;
            }
            downloadManager.resumeDownload(inputDownload);
            GUI.refreshTable();
            setVisible(false);
        });
        add(startButton, constraints);

        constraints.gridx = 2;
        JButton scheduleButton = new JButton(JDM.getText("Schedule"));
        scheduleButton.addActionListener(e -> {
            Download inputDownload;
            try {
                inputDownload = downloadManager.newDownload(new URL(urlField.getText()), saveLocation.getText());
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this, JDM.getText("Invalid URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Invalid URL");
                return;
            }
            if (downloadManager.isForbidden(inputDownload.getUrl().toString())) {
                downloadManager.removeDownload(inputDownload);
                JOptionPane.showMessageDialog(this, JDM.getText("Forbidden URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Forbidden URL");
                return;
            }
            DateTimePicker dateTimePicker = new DateTimePicker();
            //Setting Date Picker
            DatePickerSettings datePickerSettings = dateTimePicker.getDatePicker().getSettings();
            datePickerSettings.setAllowEmptyDates(false);
            datePickerSettings.setAllowKeyboardEditing(false);
            datePickerSettings.setDateRangeLimits(LocalDate.now(), LocalDate.MAX);
            //Setting Time Picker
            TimePickerSettings timePickerSettings = dateTimePicker.getTimePicker().getSettings();
            timePickerSettings.setAllowEmptyTimes(false);
            timePickerSettings.setDisplaySpinnerButtons(true);
            timePickerSettings.setDisplayToggleTimeMenuButton(true);
            timePickerSettings.setInitialTimeToNow();
            timePickerSettings.setFormatForDisplayTime(PickerUtilities.createFormatterFromPatternString("HH:mm:ss", timePickerSettings.getLocale()));
            timePickerSettings.setFormatForMenuTimes(PickerUtilities.createFormatterFromPatternString("HH:mm:ss", timePickerSettings.getLocale()));
            int choice = JOptionPane.CANCEL_OPTION;
            try {
                choice = JOptionPane.showConfirmDialog(this, dateTimePicker, JDM.getText("Choose Time To Start"), JOptionPane.OK_CANCEL_OPTION);
            } catch (Exception e1) {
                System.err.println("Unexpected Situation with Schedule Dialogue");
            }
            if (choice == JOptionPane.OK_OPTION) {
                Date chosenDate = new Date(1000 * dateTimePicker.datePicker.getDate().toEpochSecond(dateTimePicker.timePicker.getTime(), OffsetDateTime.now().getOffset()));
                scheduleManager.addSchedule(inputDownload, chosenDate);
                GUI.refreshTable();
                setVisible(false);
            }
        });
        add(scheduleButton, constraints);

        constraints.gridx = 3;
        queueButton = new JButton(JDM.getText("Add To Queue"));
        queueButton.addActionListener(e -> {
            Download inputDownload = null;

            try {
                if (saveLocation.getText().equals(downloadManager.getDefaultSaveFileLocation()))
                    inputDownload = downloadManager.newDownload(new URL(urlField.getText()), saveLocation.getText());
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(this, JDM.getText("Invalid URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Invalid URL");
                return;
            }
            assert inputDownload != null;
            if (downloadManager.isForbidden(inputDownload.getUrl().toString())) {
                downloadManager.removeDownload(inputDownload);
                JOptionPane.showMessageDialog(this, JDM.getText("Forbidden URL"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
                System.err.println("Forbidden URL");
                return;
            }
            String[] options = queueManager.getQueues();
            if (options.length == 0)
                JOptionPane.showMessageDialog(this, "No Queue is Created !", "Add To Queue", JOptionPane.ERROR_MESSAGE);
            String queueName = (String) JOptionPane.showInputDialog(this, "Choose Queue to add Download to", "Add to Queue", JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
            if (queueName != null) {
                queueManager.addDownload(queueName, inputDownload);
                GUI.refreshTable();
                setVisible(false);
            }
        });
        add(queueButton, constraints);

        constraints.gridx = 4;
        JButton closeButton = new JButton(JDM.getText("Cancel"));
        closeButton.addActionListener(e -> dispose());
        add(closeButton, constraints);

        pack();


        GUI.languageRefresher.add(() -> {
            setTitle(JDM.getText("Add New Download"));
            fileLabel.setToolTipText(JDM.getText("Click to Change location"));
            addButton.setText(JDM.getText("Add To List"));
            startButton.setText(JDM.getText("Add And Start"));
            scheduleButton.setText(JDM.getText("Schedule"));
            queueButton.setText(JDM.getText("Add To Queue"));
            closeButton.setText(JDM.getText("Cancel"));

        });
    }

    /**
     * Updates the content when window is shown
     *
     * @param b whether the window is shown or hidden
     */
    @Override
    public void setVisible(boolean b) {
        if (b) {
            queueButton.setEnabled(queueManager.getQueues().length != 0);
            saveLocation.setText(queueManager.getDownloadManager().getDefaultSaveFileLocation());
            try {
                String clipBoardText = (String) Toolkit.getDefaultToolkit().getSystemClipboard().getData(DataFlavor.stringFlavor);
                clipBoardText = clipBoardText.replaceAll("\\s*", "");
                if (clipBoardText.matches(".{3,6}//.*"))
                    urlField.setText(clipBoardText);
                else if (clipBoardText.matches("[w]{3}[.][^.]+[.][^.]+[.].+"))
                    urlField.setText("http://" + clipBoardText);
                else
                    urlField.setText(JDM.getText("Enter URL . . ."));
                urlField.setSelectionStart(0);
            } catch (UnsupportedFlavorException e) {
                System.err.println("URL Error :" + e.getMessage());
            } catch (IOException e) {
                System.err.println("ClipBoard Error");
            }

        } else
            dispose();
        super.setVisible(b);
    }

}
