package jdm.GUI;

import jdm.DownloadManager;
import jdm.JDM;

import javax.swing.*;
import java.awt.*;

/**
 * Window For Managing Filters
 */
class FilterWindow extends JFrame {

    private DownloadManager downloadManager;
    private Component parent;
    private JList<String> list;
    private JScrollPane scrollPane;

    /**
     * Initializing Window
     *
     * @param downloadManager for adding and removing filter
     * @param parent          parent component
     * @param icon            Icon of window
     */
    FilterWindow(DownloadManager downloadManager, Component parent, Image icon) {
        super(JDM.getText("Filters"));
        this.parent = parent;
        this.downloadManager = downloadManager;
        setIconImage(icon);
        setLayout(new BorderLayout());
        setAlwaysOnTop(true);
        scrollPane = new JScrollPane();
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        list = new JList<>(downloadManager.getForbiddenLinks());
        scrollPane.setViewportView(list);
        add(scrollPane, BorderLayout.CENTER);
        JPanel bottomPanel = new JPanel(new GridLayout(1, 3));
        JButton add = new JButton(JDM.getText("Add URL"));
        add.setFont(new Font("SansSerif", Font.PLAIN, 15));
        bottomPanel.add(add);
        JButton remove = new JButton(JDM.getText("Remove"));
        remove.setFont(new Font("SansSerif", Font.PLAIN, 15));
        bottomPanel.add(remove);
        JButton close = new JButton(JDM.getText("Close"));
        close.setFont(new Font("SansSerif", Font.PLAIN, 15));
        bottomPanel.add(close);
        add(bottomPanel, BorderLayout.SOUTH);
        remove.addActionListener(e -> {
            downloadManager.removeException(list.getSelectedValue());
            refresh();
        });
        add.addActionListener(e -> {
            String ex = JOptionPane.showInputDialog(parent, JDM.getText("Enter Exception URL"));
            if (!(ex == null || ex.isEmpty()))
                downloadManager.addException(ex);
            refresh();
        });
        close.addActionListener(e -> dispose());
        GUI.languageRefresher.add(() -> {
            this.setTitle(JDM.getText("Filters"));
            close.setText(JDM.getText("Close"));
            add.setText(JDM.getText("Add URL"));
            remove.setText(JDM.getText("Remove"));
        });

    }

    /**
     * Shows the window
     */
    void showWindow() {
        pack();
        setLocationRelativeTo(parent);
        setVisible(true);
        requestFocus();
    }

    /**
     * Refreshes the window if changes are made
     */
    private void refresh() {
        SwingUtilities.invokeLater(() -> {
            list = new JList<>(downloadManager.getForbiddenLinks());
            scrollPane.setViewportView(list);
            list.revalidate();
            list.repaint();
            repaint();
        });
    }
}
