package jdm.GUI;

import jdm.JDM;
import jdm.ScheduleManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

/**
 * User Interface for managing Schedules
 */
class ScheduleGUI {

    private JFrame frame;
    private JList<String> schedulesList;
    private Component parent;
    private ScheduleManager scheduleManager;

    /**
     * Initializes the interface
     *
     * @param scheduleManager manager and runner of schedules
     * @param parent          parent component
     * @param image           icon of frame
     */
    ScheduleGUI(ScheduleManager scheduleManager, Component parent, Image image) {
        this.parent = parent;
        this.scheduleManager = scheduleManager;
        //Setting Main Frame :
        frame = new JFrame(JDM.getText("Schedule"));
        frame.setIconImage(image);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setLayout(new BorderLayout(5, 5));
        frame.getContentPane().setBackground(new Color(255, 248, 228));
        JLabel topLabel = new JLabel(JDM.getText("Schedules List"));
        topLabel.setFont(new Font("SansSerif", Font.BOLD, 20));
        topLabel.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        topLabel.setHorizontalAlignment(JLabel.CENTER);
        topLabel.setBackground(new Color(255, 207, 206));
        topLabel.setForeground(new Color(42, 104, 255));
        topLabel.setHorizontalTextPosition(JLabel.CENTER);
        topLabel.setVerticalTextPosition(JLabel.CENTER);
        frame.add(topLabel, BorderLayout.NORTH);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                parent.setEnabled(true);
                parent.setFocusable(true);
                parent.requestFocus();
            }
        });

        //Setting List
        schedulesList = new JList<>();
        schedulesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        schedulesList.setModel(new ScheduleListModel(scheduleManager.getSchedules()));
        schedulesList.setFont(new Font("Arial", Font.BOLD, 14));
        JScrollPane listPane = new JScrollPane(schedulesList);
        frame.add(listPane, BorderLayout.CENTER);

        //Setting Buttons
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 2));

        JButton remove = new JButton(JDM.getText("Remove"));
        remove.setToolTipText(JDM.getText("Removes and cancel's Selected Schedule"));
        remove.addActionListener(e -> {
            int selectedIndex = schedulesList.getSelectedIndex();
            if (selectedIndex != -1) {
                scheduleManager.removeSchedule(schedulesList.getSelectedValue());
                updateList();
            }
        });
        buttonsPanel.add(remove);


        JButton close = new JButton(JDM.getText("Close"));
        close.addActionListener(e -> frame.dispose());
        buttonsPanel.add(close);

        frame.add(buttonsPanel, BorderLayout.SOUTH);

        frame.setPreferredSize(new Dimension(frame.getPreferredSize().width + 150, frame.getPreferredSize().height));
        frame.pack();

        GUI.languageRefresher.add(() -> {
            frame.setTitle(JDM.getText("Schedule"));
            topLabel.setText(JDM.getText("Schedules List"));
            remove.setText(JDM.getText("Remove"));
            remove.setToolTipText(JDM.getText("Removes and cancel's Selected Schedule"));
            close.setText(JDM.getText("Close"));
        });

    }

    /**
     * Shows the frame
     */
    void show() {
        parent.setEnabled(false);
        parent.setFocusable(false);
        updateList();
        frame.setLocationRelativeTo(parent);
        frame.setVisible(true);
    }

    /**
     * Updates the list based on changes
     */
    private void updateList() {
        SwingUtilities.invokeLater(() -> {
            ((ScheduleListModel) schedulesList.getModel()).update();
            schedulesList.ensureIndexIsVisible(scheduleManager.getSchedules().size() - 1);
            schedulesList.revalidate();
            schedulesList.repaint();
        });
    }

    /**
     * Costume model for schedules
     */
    private class ScheduleListModel extends AbstractListModel<String> {

        private Vector<String> data;

        ScheduleListModel(Vector<String> data) {
            this.data = data;
        }

        @Override
        public int getSize() {
            return data.size();
        }

        @Override
        public String getElementAt(int index) {
            return data.elementAt(index);
        }

        void update() {
            fireContentsChanged(this, 0, data.size() - 1);
            updateList();
        }

        @Override
        protected void fireContentsChanged(Object source, int index0, int index1) {
            data = scheduleManager.getSchedules();
            super.fireContentsChanged(source, index0, index1);
        }
    }

}
