package jdm.GUI;

import jdm.Download;
import jdm.JDM;
import jdm.Queue;
import jdm.QueueManager;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Vector;

/**
 * Interface for managing queues
 * Creating  & starting & stopping & modifying queues
 */
public class QueueManagerGUI {

    private static QueueManager queueManager;
    private static JTable downloadsTable;//Used to show Downloads in the selected queue
    private JFrame frame;
    private JComboBox<String> queuesComboBox;
    private JScrollPane tablePane = new JScrollPane();//Pane where table is shown
    private Component parent;

    /**
     * Initializes components
     *
     * @param queueManager manager for queues (doing operations on them)
     * @param inputParent  the parent of this window which is main panel of program
     */
    QueueManagerGUI(QueueManager queueManager, Component inputParent) {

        QueueManagerGUI.queueManager = queueManager;
        parent = inputParent;

        //Setting Frame
        frame = new JFrame(JDM.getText("Queues"));
        try {
            frame.setIconImage(ImageIO.read(new File("./resources/QueueManageIcon.png")));
        } catch (IOException e) {
            System.err.println("Couldn't load Queue Manager Frame Icon !");
        }
        frame.setComponentOrientation(parent.getComponentOrientation());
        frame.setAlwaysOnTop(true);
        frame.setLayout(new BorderLayout());

        //Setting Top Panel
        setTopPanel();

        //Setting Table
        setTable();

        //Setting Bottom Panel
        setBottomPanel();

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                parent.setFocusable(true);
                parent.setEnabled(true);
                parent.requestFocus();
            }
        });

        GUI.languageRefresher.add(() -> frame.setTitle(JDM.getText("Queues")));
    }

    /**
     * If a download is finished , this method will remove it from list (if called of course)
     */
    public static void refreshTable() {
        if (queueManager != null && downloadsTable != null) {
            for (Queue queue : queueManager.getQueuesList())
                if (queue.removeCompleted())
                    SwingUtilities.invokeLater(() -> {
                        downloadsTable.revalidate();
                        downloadsTable.repaint();
                    });
        }
    }

    /**
     * Setting Panel of Buttons
     */
    private void setBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.setBackground(new Color(229, 255, 243));
        frame.add(bottomPanel, BorderLayout.SOUTH);
        bottomPanel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.BOTH;

        gridBagConstraints.gridy = 0;
        JButton moveUp = new JButton();
        try {
            moveUp.setIcon(new ImageIcon(ImageIO.read(new File("./resources/ArrowUpIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Up Icon !");
        }
        moveUp.addActionListener(e -> {
            int selectedRow = downloadsTable.getSelectedRow();
            if (selectedRow != -1) {
                try {
                    selectedRow = queueManager.getQueues(((String) queuesComboBox.getSelectedItem())).moveUp(selectedRow);
                    downloadsTable.setRowSelectionInterval(selectedRow, selectedRow);
                    updateTable();
                } catch (NullPointerException e1) {
                    System.err.println("No Download Selected");
                }
            }
        });
        gridBagConstraints.gridx = 0;
        bottomPanel.add(moveUp, gridBagConstraints);

        JButton moveDown = new JButton();
        try {
            moveDown.setIcon(new ImageIcon(ImageIO.read(new File("./resources/ArrowDownIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Down Icon !");
        }
        moveDown.addActionListener(e -> {
            int selectedRow = downloadsTable.getSelectedRow();
            if (selectedRow != -1) {
                try {
                    selectedRow = queueManager.getQueues(((String) queuesComboBox.getSelectedItem())).moveDown(selectedRow);
                    downloadsTable.setRowSelectionInterval(selectedRow, selectedRow);
                    updateTable();
                } catch (NullPointerException e1) {
                    System.err.println("No Download Selected");
                }
            }
        });
        gridBagConstraints.gridx = 1;
        bottomPanel.add(moveDown, gridBagConstraints);

        JButton delete = new JButton();
        try {
            delete.setIcon(new ImageIcon(ImageIO.read(new File("./resources/CrossIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Delete Icon !");
        }
        delete.addActionListener(e -> {
            int selectedRow = downloadsTable.getSelectedRow();
            if (selectedRow != -1) {
                try {
                    queueManager.getQueues(((String) queuesComboBox.getSelectedItem())).remove(selectedRow);
                    updateTable();
                } catch (NullPointerException e1) {
                    System.err.println("No Download Selected");
                }
            }
        });
        gridBagConstraints.gridx = 2;
        bottomPanel.add(delete, gridBagConstraints);

        JButton start = new JButton(JDM.getText("Start Queue"));
        start.addActionListener(e -> {
            try {
                queueManager.getQueues(((String) queuesComboBox.getSelectedItem())).startDownloading();
                updateTable();
                GUI.refreshTable();
            } catch (NullPointerException e1) {
                System.err.println("No Queue Selected");
            }
        });
        gridBagConstraints.gridx = 4;
        gridBagConstraints.weightx = 1;
        bottomPanel.add(start, gridBagConstraints);

        JButton stop = new JButton(JDM.getText("Stop Queue"));
        stop.addActionListener(e -> {
            try {
                queueManager.getQueues(((String) queuesComboBox.getSelectedItem())).stopDownloading();
            } catch (NullPointerException e1) {
                System.err.println("No Queue Selected");
            }
        });
        gridBagConstraints.gridx = 5;
        bottomPanel.add(stop, gridBagConstraints);

        JButton clear = new JButton(JDM.getText("Clear Queue"));
        clear.setToolTipText(JDM.getText("Cleans the queue (Remove all downloads)"));
        clear.addActionListener(e -> {
            try {
                queueManager.getQueues(((String) queuesComboBox.getSelectedItem())).remove();
                updateTable();
            } catch (NullPointerException e1) {
                System.err.println("No Queue Selected");
            }
        });
        gridBagConstraints.gridx = 6;
        bottomPanel.add(clear, gridBagConstraints);

        GUI.languageRefresher.add(() -> {
            start.setText(JDM.getText("Start Queue"));
            stop.setText(JDM.getText("Stop Queue"));
            clear.setText(JDM.getText("Clear Queue"));
            clear.setToolTipText(JDM.getText("Cleans the queue (Remove all downloads)"));
            updateComboBox();
        });

    }

    /**
     * Setting list of downloads in a queue
     */
    private void setTable() {
        downloadsTable = new JTable();
        downloadsTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tablePane = new JScrollPane();
        tablePane.setViewportView(downloadsTable);
        try {
            downloadsTable.setModel(new queueDownloadTableModel(((String) queuesComboBox.getSelectedItem())));
        } catch (NullPointerException e) {
//            System.out.println("Not Any Queue");
        }
        frame.add(tablePane, BorderLayout.CENTER);
    }

    /**
     * Setting top part of the window
     */
    private void setTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        frame.add(topPanel, BorderLayout.NORTH);
        topPanel.setBackground(new Color(255, 248, 228));
        topPanel.setLayout(new GridBagLayout());
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        JLabel label = new JLabel(JDM.getText("Queues :"));
        topPanel.add(label, gridBagConstraints);

        queuesComboBox = new JComboBox<>();
        updateComboBox();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.weightx = 1.5;
        topPanel.add(queuesComboBox, gridBagConstraints);
        queuesComboBox.addActionListener(e -> updateTable());


        JButton newQueue = new JButton(JDM.getText("Create Queue"));
        gridBagConstraints.gridx = 3;
        gridBagConstraints.weightx = 1;

        newQueue.addActionListener(e -> {
            String name = (String) JOptionPane.showInputDialog(frame, JDM.getText("Name of Queue :"), JDM.getText("Create Queue"), JOptionPane.QUESTION_MESSAGE, null, null, ("Queue Number" + (queueManager.getQueues().length + 1)));
            if (name != null) {
                try {
                    queueManager.createQueue(name);
                } catch (IllegalCallerException e1) {
                    System.err.println(e1.getMessage());
                }
                updateComboBox();
            }
        });
        topPanel.add(newQueue, gridBagConstraints);

        JButton removeQueue = new JButton(JDM.getText("Remove Queue"));
        gridBagConstraints.gridx = 4;
        topPanel.add(removeQueue, gridBagConstraints);
        removeQueue.setToolTipText(JDM.getText("Removes Selected Queue"));
        removeQueue.addActionListener(e -> {
            String selectedItem = (String) queuesComboBox.getSelectedItem();
            if (selectedItem != null && !selectedItem.equals(JDM.getText("No Queue"))) {
                queueManager.removeQueue(selectedItem);
                updateComboBox();
            }
        });

        GUI.languageRefresher.add(() -> {
            label.setText(JDM.getText("Queues :"));
            newQueue.setText(JDM.getText("Create Queue"));
            removeQueue.setText(JDM.getText("Remove Queue"));
        });
    }

    /**
     * Updates the content of list according to combo boxes's choice
     */
    private void updateComboBox() {
        queuesComboBox.setModel(new DefaultComboBoxModel<>(queueManager.getQueues()));
        if (queuesComboBox.getItemCount() == 0) {
            queuesComboBox.setModel(new DefaultComboBoxModel<>(new String[]{JDM.getText("No Queues")}));
        }
        updateTable();
        SwingUtilities.invokeLater(() -> {
            queuesComboBox.revalidate();
            queuesComboBox.repaint();
        });
    }

    /**
     * Refreshes the table
     */
    private void updateTable() {
        try {
            String selectedItem = (String) queuesComboBox.getSelectedItem();
            if (selectedItem == null || selectedItem.equals("No Queue")) {
                downloadsTable.setModel(new queueDownloadTableModel(""));
            } else {
                int selectedRow = downloadsTable.getSelectedRow();
                downloadsTable.setModel(new queueDownloadTableModel(selectedItem));
                DefaultTableCellRenderer customRenderer = new DefaultTableCellRenderer();
                customRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
                downloadsTable.setAlignmentX(Component.CENTER_ALIGNMENT);
                downloadsTable.getColumn("Size").setCellRenderer(customRenderer);
                downloadsTable.getColumn("Time Added").setPreferredWidth(100);
                downloadsTable.getColumn("Time Added").setCellRenderer(customRenderer);
                tablePane.setViewportView(downloadsTable);
                try {
                    downloadsTable.setRowSelectionInterval(selectedRow, selectedRow);
                } catch (IllegalArgumentException | NullPointerException e) {
                    //Do nothing
                }
                SwingUtilities.invokeLater(() -> {
                    downloadsTable.revalidate();
                    downloadsTable.repaint();
                });
            }
        } catch (NullPointerException e) {
//            System.out.println("Not Any Queue");
            clearTablePane();
        }
    }

    /**
     * Hides the list when no queue is made
     */
    private void clearTablePane() {
        tablePane.setViewportView(null);
        SwingUtilities.invokeLater(() -> tablePane.repaint());
    }

    /**
     * Shows the window
     */
    void show() {
        parent.setFocusable(false);
        parent.setEnabled(false);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(parent);
        frame.setVisible(true);
    }

    /**
     * Costume table model for showing downloads in a queue
     */
    private class queueDownloadTableModel extends AbstractTableModel {
        private static final int COLUMN_FILENAME = 0;
        private static final int COLUMN_SIZE = 1;
        private static final int COLUMN_TIME_ADDED = 2;
        private static final int COLUMN_URL = 3;


        private String[] columnNames = {"Filename", "Size", "Time Added", "URL"};
        private Vector<Download> downloads;

        queueDownloadTableModel(String queueName) {
            downloads = queueManager.getQueues(queueName).getDownloads();
        }

        @Override
        public int getRowCount() {
            return downloads.size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex > 3)
                throw new IllegalArgumentException("Illegal Column Index");
            try {
                Download download = downloads.get(rowIndex);
                switch (columnIndex) {
                    case COLUMN_FILENAME:
                        return download.getFileName();
                    case COLUMN_SIZE:
                        return BigDecimal.valueOf(download.getSize() * 1e-3).setScale(2, RoundingMode.HALF_UP).doubleValue();
                    case COLUMN_TIME_ADDED:
                        return download.getStartTime();
                    case COLUMN_URL:
                        return download.getUrl();
                    default:
                        throw new IllegalArgumentException("Illegal Column Index");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.err.println("Column " + columnIndex + " Row " + rowIndex + " is illegal");
            }
            return null;
        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return downloads.isEmpty() ? Object.class : getValueAt(0, columnIndex).getClass();
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

    }

}
