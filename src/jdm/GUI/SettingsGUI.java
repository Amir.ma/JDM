package jdm.GUI;

import jdm.DownloadManager;
import jdm.JDM;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * Showing the interface for setting in program
 */
class SettingsGUI {

    private DownloadManager downloadManager;

    private JFrame frame;
    private JComboBox<String> simultaneousDownloads;
    private JComboBox<String> lookAndFeels;
    private JComboBox<Integer> numberOfSegments;
    private JComboBox<String> languages;
    private JTextField defaultSaveLocation;
    private Component parent;

    /**
     * Sets different part of window
     *
     * @param downloadManager class for managing downloads
     * @param parentInput     parent of this window , which is main frame of program
     */
    SettingsGUI(DownloadManager downloadManager, Component parentInput) {
        this.parent = parentInput;
        this.downloadManager = downloadManager;
        frame = new JFrame(JDM.getText("Settings"));
        frame.setBackground(new Color(255, 252, 246));
        frame.getContentPane().setBackground(new Color(250, 255, 241));
        try {
            frame.setIconImage(ImageIO.read(new File("./resources/SettingsIcon.png")));
        } catch (IOException e) {
            System.err.println("Couldn't Load Setting Icon");
        }
        frame.setLayout(new GridLayout(6, 2, 8, 5));

        //Setting Number of Sim Downloads Part
        String[] allowed = new String[21];
        allowed[0] = JDM.getText("Unlimited");
        for (int i = 1; i < 21; i++) {
            allowed[i] = "" + i;
        }
        simultaneousDownloads = new JComboBox<>(allowed);
        simultaneousDownloads.setFont(new Font("SansSerif", Font.PLAIN, 15));
        simultaneousDownloads.setSelectedItem(downloadManager.getNumberOfSimultaneousDownloads() == DownloadManager.UNLIMITED_NUMBER_OF_SIMULTANEOUS_DOWNLOADS ? JDM.getText("Unlimited") : "" + downloadManager.getNumberOfSimultaneousDownloads());
        simultaneousDownloads.addActionListener(e -> setSimultaneousDownloads());
        JLabel label = new JLabel(JDM.getText("Number of Simultaneous downloads"));
        label.setFont(new Font("SansSerif", Font.BOLD, 16));
        label.setHorizontalAlignment(JLabel.CENTER);
        frame.add(label);
        frame.add(simultaneousDownloads);

        //Setting Number of Segments
        Integer[] allowed2 = new Integer[8];
        for (int i = 0; i < 8; i++) {
            allowed2[i] = i + 1;
        }
        numberOfSegments = new JComboBox<>(allowed2);
        numberOfSegments.setFont(new Font("SansSerif", Font.PLAIN, 15));
        numberOfSegments.setSelectedItem(downloadManager.getNumberOfSegments());
        numberOfSegments.addActionListener(e -> setNumberOfSegments());
        JLabel label4 = new JLabel(JDM.getText("Number of Segments for Downloading"));
        label4.setFont(new Font("SansSerif", Font.BOLD, 16));
        label4.setHorizontalAlignment(JLabel.CENTER);
        frame.add(label4);
        frame.add(numberOfSegments);

        //Setting Choosing L&F Part
        UIManager.LookAndFeelInfo[] installedLookAndFeels = UIManager.getInstalledLookAndFeels();
        String[] lookAndFeelsStrings = new String[installedLookAndFeels.length];
        String[] lookAndFeelsNames = new String[lookAndFeelsStrings.length];
        int i = 0;
        for (UIManager.LookAndFeelInfo lookAndFeel : installedLookAndFeels) {
            lookAndFeelsNames[i] = lookAndFeel.getName();
            lookAndFeelsStrings[i] = lookAndFeel.getClassName();
            i++;
        }
        lookAndFeels = new JComboBox<>(lookAndFeelsNames);
        lookAndFeels.setFont(new Font("SansSerif", Font.PLAIN, 15));
        lookAndFeels.addActionListener(e -> {
            try {
                String lookAndFeelsString = lookAndFeelsStrings[lookAndFeels.getSelectedIndex()];
                UIManager.setLookAndFeel(lookAndFeelsString);

                for (Window window : Window.getWindows())//Changing UI for all windows
                    SwingUtilities.updateComponentTreeUI(window);
                downloadManager.setLookAndFeel(lookAndFeelsString);//Saving change

            } catch (UnsupportedLookAndFeelException | IllegalAccessException | ClassNotFoundException | InstantiationException exception) {
                System.err.println("Couldn't set Look And Feel " + exception.getMessage());
            }
        });
        lookAndFeels.setSelectedItem(UIManager.getLookAndFeel().getName());
        JLabel label1 = new JLabel(JDM.getText("Look And Feels"));
        label1.setFont(new Font("SansSerif", Font.BOLD, 20));
        label1.setHorizontalAlignment(JLabel.CENTER);
        frame.add(label1);
        frame.add(lookAndFeels);


        //Setting Choosing Default Save Location Part
        JPanel fileChoosingPanel = new JPanel();
        fileChoosingPanel.setLayout(new BorderLayout());
        defaultSaveLocation = new JTextField(downloadManager.getDefaultSaveFileLocation());
        defaultSaveLocation.setFont(new Font("SansSerif", Font.PLAIN, 14));
        defaultSaveLocation.setEditable(false);
        defaultSaveLocation.setPreferredSize(new Dimension(40, 20));
        defaultSaveLocation.setCaretPosition(0);
        JButton chooseLocation = new JButton(JDM.getText("Change"));
        chooseLocation.setFont(new Font("SansSerif", Font.BOLD, 13));
        chooseLocation.addActionListener(e -> setDefaultLocation());
        fileChoosingPanel.add(chooseLocation, BorderLayout.EAST);
        fileChoosingPanel.add(defaultSaveLocation, BorderLayout.CENTER);
        JLabel label2 = new JLabel(JDM.getText("Default Save Location"));
        label2.setHorizontalAlignment(JLabel.CENTER);
        label2.setFont(new Font("SansSerif", Font.BOLD, 20));
        frame.add(label2);
        frame.add(fileChoosingPanel);

        //Change Language Part
        String[] languagesStrings = new File("Languages/").list();
        assert languagesStrings != null;
        //Removing .txt 's
        Arrays.setAll(languagesStrings, j -> languagesStrings[j].replaceAll(".txt", ""));

        languages = new JComboBox<>(languagesStrings);
        languages.setFont(new Font("SansSerif", Font.PLAIN, 15));
        languages.addActionListener(e -> {
            String item = (String) languages.getSelectedItem();
            if (item == null)
                return;
            if (!item.equals(JDM.language))
                JDM.loadLanguage(item);
            GUI.refreshLanguage();
            for (Window window : Window.getWindows())//Changing UI for all windows
                window.repaint();
        });
        languages.setSelectedItem(JDM.language == null ? "English" : JDM.language);
        JLabel label3 = new JLabel(JDM.getText("Change Language"));
        label3.setFont(new Font("SansSerif", Font.BOLD, 20));
        label3.setHorizontalAlignment(JLabel.CENTER);
        frame.add(label3);
        frame.add(languages);


        //Filter creating Part
        JLabel filterLabel = new JLabel();
        try {
            ImageIcon imageIcon = new ImageIcon(ImageIO.read(new File("./resources/FilterIcon.png")));
            filterLabel.setIcon(imageIcon);
            final FilterWindow filterWindow = new FilterWindow(downloadManager, frame, imageIcon.getImage());
            filterLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    filterWindow.showWindow();
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    filterLabel.setBackground(Color.CYAN);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    filterLabel.setBackground(frame.getContentPane().getBackground());
                }
            });
        } catch (IOException e) {
            System.err.println("Couldn't set Filter Window");
        }
        filterLabel.setHorizontalAlignment(SwingConstants.CENTER);
        filterLabel.setHorizontalTextPosition(SwingConstants.TRAILING);
        filterLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
        filterLabel.setOpaque(true);
        filterLabel.setText(JDM.getText("Filters"));
        filterLabel.setFont(new Font("SansSerif", Font.BOLD, 18));
        filterLabel.setToolTipText(JDM.getText("Manage Filters for URL's"));


        frame.add(filterLabel);


        //CloseButton
        JButton close = new JButton(JDM.getText("Close"));
        close.setFont(new Font("SansSerif", Font.BOLD, 20));
        close.addActionListener(e -> frame.dispose());
        frame.add(close);
        frame.setAlwaysOnTop(true);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                parent.setFocusable(true);
                parent.setEnabled(true);
                parent.requestFocus();
            }
        });


        GUI.languageRefresher.add(() -> {
            allowed[0] = JDM.getText("Unlimited");
            simultaneousDownloads.setSelectedItem(downloadManager.getNumberOfSimultaneousDownloads() == DownloadManager.UNLIMITED_NUMBER_OF_SIMULTANEOUS_DOWNLOADS ? JDM.getText("Unlimited") : "" + downloadManager.getNumberOfSimultaneousDownloads());
            label.setText(JDM.getText("Number of Simultaneous downloads"));
            label1.setText(JDM.getText("Look And Feels"));
            chooseLocation.setText(JDM.getText("Change"));
            label2.setText(JDM.getText("Default Save Location"));
            label3.setText(JDM.getText("Change Language"));
            filterLabel.setText(JDM.getText("Filters"));
            filterLabel.setToolTipText(JDM.getText("Manage Filters for URL's"));
            close.setText(JDM.getText("Close"));
            allowed[0] = JDM.getText("Unlimited");
            simultaneousDownloads.setModel(new DefaultComboBoxModel<>(allowed));
            simultaneousDownloads.setSelectedItem(downloadManager.getNumberOfSimultaneousDownloads() == DownloadManager.UNLIMITED_NUMBER_OF_SIMULTANEOUS_DOWNLOADS ? JDM.getText("Unlimited") : "" + downloadManager.getNumberOfSimultaneousDownloads());
        });

        frame.pack();
//        frame.setSize(new Dimension(frame.getPreferredSize().width-100,frame.getPreferredSize().height));
    }

    /**
     * Showing the GUI
     */
    void showFrame() {
        parent.setFocusable(false);
        parent.setFocusTraversalKeysEnabled(false);
        parent.setEnabled(false);
        defaultSaveLocation.setText(downloadManager.getDefaultSaveFileLocation());
        frame.requestFocusInWindow();
        frame.setLocationRelativeTo(parent);
        frame.setVisible(true);
    }

    /**
     * Changing the default save file location
     */
    private void setDefaultLocation() {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(true);
        try {
            fileChooser.setCurrentDirectory(new File(downloadManager.getDefaultSaveFileLocation()));
        } catch (NullPointerException e) {
            fileChooser.setCurrentDirectory(Paths.get(".").toAbsolutePath().toFile());
        }
        if (fileChooser.showDialog(frame, JDM.getText("Choose Location")) == JFileChooser.APPROVE_OPTION) {
            System.out.println("Default Save File Location set to -> " + fileChooser.getSelectedFile());
            String chosenPath = fileChooser.getSelectedFile().toString();
            downloadManager.setDefaultSaveFileLocation(chosenPath);
            defaultSaveLocation.setText(chosenPath);
        } else {
            System.err.println("No Selection");
        }
    }

    /**
     * Changing number of simultaneous downloads
     */
    private void setSimultaneousDownloads() {
        downloadManager.setNumberOfSimultaneousDownloads(simultaneousDownloads.getSelectedIndex() == 0 ? DownloadManager.UNLIMITED_NUMBER_OF_SIMULTANEOUS_DOWNLOADS : simultaneousDownloads.getSelectedIndex());
        System.out.println("Number Of Simultaneous Downloads set to -> "
                + (downloadManager.getNumberOfSimultaneousDownloads() == DownloadManager.UNLIMITED_NUMBER_OF_SIMULTANEOUS_DOWNLOADS ? JDM.getText("Unlimited") : downloadManager.getNumberOfSimultaneousDownloads()));
    }

    /**
     * Changes number of segments for downloading files (can be changed for a download download)
     */
    private void setNumberOfSegments() {
        Object selectedItem = numberOfSegments.getSelectedItem();
        if (selectedItem instanceof Integer)
            downloadManager.setNumberOfSegments(((Integer) selectedItem));
        System.out.println("Number Of Segments  set to -> " + downloadManager.getNumberOfSegments());
    }

}
