package jdm.GUI;

import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.components.DateTimePicker;
import com.github.lgooddatepicker.components.TimePickerSettings;
import com.github.lgooddatepicker.optionalusertools.PickerUtilities;
import jdm.*;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Main GUI of the program
 */
public class GUI {

    static ArrayList<Runnable> languageRefresher;//Refreshing works need to be done after language changed
    private static JTable downloadTable;
    private JFrame frame;
    private JToolBar topToolBar;
    private JToolBar sideToolBar;
    private JMenuBar menuBar;
    private SettingsGUI settingsGUI;
    private DownloadManager downloadManager;
    private QueueManager queueManager;
    private ScheduleManager scheduleManager;

    /**
     * Initializes the main frame
     *
     * @param queueManager    manager of queues
     * @param downloadManager manager of downloads
     * @param scheduleManager scheduler
     */
    public GUI(QueueManager queueManager, DownloadManager downloadManager, ScheduleManager scheduleManager) {
        this.scheduleManager = scheduleManager;
        this.queueManager = queueManager;
        this.downloadManager = downloadManager;

        languageRefresher = new ArrayList<>(5);

        //Setting Look and Feel
        try {
            if (downloadManager.getLookAndFeel() != null && !downloadManager.getLookAndFeel().isEmpty())
                UIManager.setLookAndFeel(downloadManager.getLookAndFeel());
            else {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            }
        } catch (ClassNotFoundException | InstantiationException | UnsupportedLookAndFeelException | IllegalAccessException e) {
            System.err.println(e.getMessage());
        }

        //Setting Frame and Main panel
        frame = new JFrame();

        languageRefresher.add(() -> frame.setTitle(JDM.getText("Java Download Manager")));

        try {
            frame.setIconImage(ImageIO.read(new File("./resources/JDMIcon.png")));
        } catch (Exception e) {
            System.err.println("Couldn't load Frame Icon !");
        }
        JPanel mainPanel = new JPanel(new BorderLayout(10, 10));
        mainPanel.setBackground(new Color(184, 214, 229));
        frame.setContentPane(mainPanel);
        mainPanel.requestFocusInWindow();


        //Setting Panel for Download List
        DownloadTableModel tableModel = new DownloadTableModel();
        downloadTable = new JTable(tableModel);

        //Refreshing language of columns if needed
        languageRefresher.add(() -> {
            DownloadTableModel model = (DownloadTableModel) downloadTable.getModel();
            Iterator<TableColumn> iterator = downloadTable.getColumnModel().getColumns().asIterator();
            TableColumn column;
            while (iterator.hasNext()) {
                column = iterator.next();
                column.setHeaderValue(JDM.getText(model.getText(column.getModelIndex())));
            }
            downloadTable.getTableHeader().resizeAndRepaint();
        });

        downloadTable.setBackground(Color.WHITE);
        DefaultTableCellRenderer customRenderer = new DefaultTableCellRenderer();
        customRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER);
        downloadTable.setAlignmentX(Component.CENTER_ALIGNMENT);
        downloadTable.getColumn("Size").setCellRenderer(customRenderer);
        downloadTable.getColumn("Size").setPreferredWidth(100);
        downloadTable.getColumn("Progress").setCellRenderer(customRenderer);
        downloadTable.getColumn("Time Added").setCellRenderer(customRenderer);
        downloadTable.getColumn("Speed").setCellRenderer(customRenderer);
        downloadTable.getColumn("Speed").setPreferredWidth(100);
        downloadTable.getColumn("Filename").setPreferredWidth(200);
        downloadTable.getColumn("Time Added").setPreferredWidth(200);
        downloadTable.getColumn("URL").setPreferredWidth(250);
        downloadTable.getColumnModel().getColumn(2).setCellRenderer(new ProgressBarRenderer());
        JScrollPane downloadListPane = new JScrollPane(downloadTable);
        downloadTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        downloadTable.setAutoCreateRowSorter(true);
        mainPanel.add(downloadListPane, BorderLayout.CENTER);


        //Setting ToolBars
        topToolBar = new JToolBar("Top", JToolBar.HORIZONTAL);
        topToolBar.setBackground(new Color(114, 136, 154));
        topToolBar.setRollover(true);
        topToolBar.setBorder(new LineBorder(new Color(255, 211, 0), 5, true));
        sideToolBar = new JToolBar("", JToolBar.VERTICAL);
        sideToolBar.setBackground(new Color(43, 97, 167));
        sideToolBar.setRollover(true);
        sideToolBar.setBorder(new LineBorder(new Color(236, 255, 0), 5, true));
        mainPanel.add(topToolBar, BorderLayout.BEFORE_FIRST_LINE);
        mainPanel.add(sideToolBar, BorderLayout.WEST);

        PropertyChangeListener listener = evt -> {
            topToolBar.validate();
            topToolBar.repaint();
            frame.pack();
        };
        topToolBar.addPropertyChangeListener(listener);
        sideToolBar.addPropertyChangeListener(listener);


        //Setting MenuBar and its items
        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        setMenuItems();

        //Setting Buttons
        setNewDownload();
        topToolBar.addSeparator();
        setResume();
        topToolBar.addSeparator();
        setPause();
        topToolBar.addSeparator();
        setCancel();
        topToolBar.addSeparator();
        setRemove();
        topToolBar.addSeparator();
        setSearch();
        setSettings();
        sideToolBar.addSeparator();
        setQueueManager();
        sideToolBar.addSeparator();
        setScheduler();

        //Modifying Tool Bars Colors
        for (Component[] components : Arrays.asList(topToolBar.getComponents(), sideToolBar.getComponents()))
            for (Component component : components) {
                component.setForeground(new Color(0xFFFFFF - component.getBackground().getRGB()));
                component.setFont(new Font("SansSerif", Font.BOLD, 15));
            }

        //Setting click on table stuff
        setTableClick();

        //Setting Tray Icon
        if (SystemTray.isSupported()) {
            setTray();
        } else
            System.err.println("System Tray not Supported");

    }

    /**
     * Refreshed table
     * According to list of downloads
     */
    public static void refreshTable() {
        SwingUtilities.invokeLater(() -> {
            if (downloadTable.getSelectionModel().getSelectionMode() == ListSelectionModel.MULTIPLE_INTERVAL_SELECTION) {
                downloadTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            }
            downloadTable.getRowSorter().allRowsChanged();
            downloadTable.revalidate();
            downloadTable.repaint();
        });
    }

    /**
     * Refreshes language by calling all refreshers
     */
    static void refreshLanguage() {
        for (Runnable runnable : languageRefresher)
            runnable.run();
    }

    /**
     * Setting Menu
     */
    private void setMenuItems() {
        JMenu tasksMenu = menuBar.add(new JMenu(JDM.getText("Tasks")));
        JMenuItem newDownload = tasksMenu.add(new JMenuItem(JDM.getText("New Download")));
        newDownload.setMnemonic('n');
        newDownload.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_DOWN_MASK));
        JMenuItem settings = tasksMenu.add(new JMenuItem(JDM.getText("Settings")));
        settings.setMnemonic('s');
        settings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
        JMenuItem export = tasksMenu.add(new JMenuItem(JDM.getText("Export")));
        export.setMnemonic('x');
        export.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, KeyEvent.CTRL_DOWN_MASK));
        export.addActionListener(e -> {
            String saveLocation;
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(true);
            fileChooser.setCurrentDirectory(new File(downloadManager.getDefaultSaveFileLocation()));
            if (fileChooser.showDialog(frame, JDM.getText("Choose Location to Export")) == JFileChooser.APPROVE_OPTION) {
                saveLocation = fileChooser.getSelectedFile().toString();
                JDM.exitProgram(false);
                try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(saveLocation + "/OUT.zip"))) {
                    zipOutputStream.putNextEntry(new ZipEntry("Downloads.jdm"));
                    zipOutputStream.write(Files.readAllBytes(new File("SavedData/Downloads.jdm").toPath()));
                    zipOutputStream.closeEntry();
                    zipOutputStream.putNextEntry(new ZipEntry("Queues.jdm"));
                    zipOutputStream.write(Files.readAllBytes(new File("SavedData/Queues.jdm").toPath()));
                    zipOutputStream.closeEntry();
                    zipOutputStream.putNextEntry(new ZipEntry("Schedule.jdm"));
                    zipOutputStream.write(Files.readAllBytes(new File("SavedData/Schedule.jdm").toPath()));
                    zipOutputStream.closeEntry();
                } catch (IOException e1) {
                    System.err.println("Error Creating Zip File. " + e1.getMessage());
                }
            } else
                System.err.println("No Selection");
        });
        JMenuItem exit = tasksMenu.add(new JMenuItem(JDM.getText("Exit")));
        exit.setMnemonic('e');
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK));
        exit.addActionListener(e -> JDM.exitProgram(true));

        JMenu downloadMenu = menuBar.add(new JMenu(JDM.getText("Download")));
        JMenuItem resumeDownloadItem = downloadMenu.add(new JMenuItem(JDM.getText("Resume")));
        resumeDownloadItem.setMnemonic('r');
        resumeDownloadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_DOWN_MASK));
        JMenuItem pauseDownloadItem = downloadMenu.add(new JMenuItem(JDM.getText("Pause")));
        pauseDownloadItem.setMnemonic('p');
        pauseDownloadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK));
        JMenuItem cancelDownloadItem = downloadMenu.add(new JMenuItem(JDM.getText("Cancel")));
        cancelDownloadItem.setMnemonic('c');
        cancelDownloadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        JMenuItem removeDownloadItem = downloadMenu.add(new JMenuItem(JDM.getText("Remove")));
        removeDownloadItem.setMnemonic('d');
        removeDownloadItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.CTRL_DOWN_MASK));


        JMenu aboutMenu = menuBar.add(new JMenu(JDM.getText("About")));
        JMenuItem aboutItem = aboutMenu.add(new JMenuItem(JDM.getText("About")));
        aboutItem.setMnemonic('a');
        aboutItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.ALT_DOWN_MASK));
        aboutItem.addActionListener(e -> {
                    if (JDM.language.equals("English"))
                        JOptionPane.showMessageDialog(frame
                                , "By Amir Mohammad Aghapour\n " +
                                        "#ID 9631008\n " +
                                        "Started at 26 April 2018\n " +
                                        "Finished at 11 May 2018\n "
                                , "About",
                                JOptionPane.INFORMATION_MESSAGE);
                    else
                        JOptionPane.showMessageDialog(frame
                                , JDM.getText("AboutDialogueMessage")
                                , JDM.getText("About"),
                                JOptionPane.INFORMATION_MESSAGE);

                }
        );
        JMenuItem helpItem = aboutMenu.add(new JMenuItem(JDM.getText("Help")));
        helpItem.addActionListener(e -> {
            if (JDM.language.equals("English"))
                JOptionPane.showMessageDialog(frame,
                        "You can add your downloads and Manage them by putting them in queues.\n" +
                                "Also queues can be start at specified time by scheduler.\n" +
                                "\tHappy Downloading :)",
                        "Help",
                        JOptionPane.INFORMATION_MESSAGE
                );
            else
                JOptionPane.showMessageDialog(frame,
                        JDM.getText("HelpDialogueMessage"),
                        JDM.getText("Help"),
                        JOptionPane.INFORMATION_MESSAGE
                );
        });
        helpItem.setMnemonic('h');
        helpItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.ALT_DOWN_MASK));

        languageRefresher.add(() -> {
            tasksMenu.setText(JDM.getText("Tasks"));
            newDownload.setText(JDM.getText("New Download"));
            settings.setText(JDM.getText("Settings"));
            export.setText(JDM.getText("Export"));
            helpItem.setText(JDM.getText("Help"));
            aboutMenu.setText(JDM.getText("About"));
            aboutItem.setText(JDM.getText("About"));
            downloadMenu.setText(JDM.getText("Download"));
            resumeDownloadItem.setText(JDM.getText("Resume"));
            pauseDownloadItem.setText(JDM.getText("Pause"));
            cancelDownloadItem.setText(JDM.getText("Cancel"));
            removeDownloadItem.setText(JDM.getText("Remove"));
            exit.setText(JDM.getText("Exit"));
        });

    }

    /**
     * Shows the program
     */
    public void showGUI() {
        //Final Works on frame
        frame.setPreferredSize(new Dimension(frame.getPreferredSize().width + 150, frame.getPreferredSize().height));
        frame.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        frame.setDefaultCloseOperation(SystemTray.isSupported() ? JFrame.DO_NOTHING_ON_CLOSE : JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    /**
     * Sets the tray icon
     */
    private void setTray() {
        SystemTray tray = SystemTray.getSystemTray();
        final TrayIcon trayIcon;
        try {
            trayIcon = new TrayIcon(frame.getIconImage(), JDM.getText("Java Download Manager"));
        } catch (NullPointerException e) {
            System.err.println("Couldn't Set Tray Icon");
            return;
        }
        PopupMenu popupMenu = new PopupMenu();
        MenuItem item = new MenuItem(JDM.getText("Exit"));
        item.addActionListener(e -> JDM.exitProgram(true));
        popupMenu.add(item);
        trayIcon.setPopupMenu(popupMenu);

        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    tray.remove(trayIcon);
                    frame.setExtendedState(JFrame.NORMAL);
                    frame.setVisible(true);
                }
            }
        });
        trayIcon.setImageAutoSize(true);
        frame.addWindowStateListener(e -> {
            switch (e.getNewState()) {
                case JFrame.ICONIFIED:
                    try {
                        tray.add(trayIcon);
                    } catch (AWTException exception) {
                        System.err.println(exception.getMessage());
                    }
                    frame.setVisible(false);
                    break;
                case JFrame.MAXIMIZED_BOTH:
                case JFrame.NORMAL:
                    tray.remove(trayIcon);
                    frame.setVisible(true);
                    break;
            }
        });
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                frame.setExtendedState(JFrame.ICONIFIED);
            }
        });

        languageRefresher.add(() -> {
            trayIcon.setToolTip(JDM.getText("Java Download Manager"));
            item.setLabel(JDM.getText("Exit"));
        });
    }

    /**
     * Action listeners when clicked on table
     */
    private void setTableClick() {
        Icon messageIcon = null;
        try {
            messageIcon = new ImageIcon(ImageIO.read(new File("./resources/DownloadInfoIcon.png")));
        } catch (IOException e) {
            System.err.println("Couldn't load Message icon");
        }
        Icon finalMessageIcon = messageIcon;


        JPopupMenu popupMenu = new JPopupMenu();
        JMenuItem showInfo = new JMenuItem(JDM.getText("Show Info"));
        showInfo.addActionListener(e -> {
            int selectedRow = downloadTable.getSelectedRow();
            if (selectedRow == -1) {
                JOptionPane.showMessageDialog(frame, JDM.getText("No Selection !"), JDM.getText("Error"), JOptionPane.ERROR_MESSAGE);
            } else {
                Download selectedDownload = downloadManager.get(downloadTable.convertRowIndexToModel(selectedRow));
                JOptionPane.showMessageDialog(frame,
                        JDM.getText("File Name") + " : " + selectedDownload.getFileName() + "\n"
                                + JDM.getText("File Size") + " : " + selectedDownload.getSize() + " Bytes" + "\n"
                                + JDM.getText("URL") + " : " + selectedDownload.getUrl() + "\n"
                                + JDM.getText("Save Location") + " : " + selectedDownload.getFileLocation() + "\n"
                                + JDM.getText("Time Added") + " : " + selectedDownload.getStartTime() + "\n"
                                + JDM.getText("Downloaded Size") + " : " + selectedDownload.getDownloadedSize(),
                        JDM.getText("Download Info"),
                        JOptionPane.PLAIN_MESSAGE,
                        finalMessageIcon);
            }
        });
        popupMenu.add(showInfo);

        JMenu addToQueue = new JMenu(JDM.getText("Add to Queue"));
        popupMenu.add(addToQueue);

        JMenuItem addToSchedule = new JMenuItem(JDM.getText("Schedule"));
        addToSchedule.addActionListener(e -> {
            int selectedRow = downloadTable.getSelectedRow();
            if (selectedRow != -1) {
                Download selectedDownload = downloadManager.get(downloadTable.convertRowIndexToModel(selectedRow));
                DateTimePicker dateTimePicker = new DateTimePicker();
                //Setting Date Picker
                DatePickerSettings datePickerSettings = dateTimePicker.getDatePicker().getSettings();
                datePickerSettings.setAllowEmptyDates(false);
                datePickerSettings.setAllowKeyboardEditing(false);
                datePickerSettings.setDateRangeLimits(LocalDate.now(), LocalDate.MAX);
                //Setting Time Picker
                TimePickerSettings timePickerSettings = dateTimePicker.getTimePicker().getSettings();
                timePickerSettings.setAllowEmptyTimes(false);
                timePickerSettings.setDisplaySpinnerButtons(true);
                timePickerSettings.setDisplayToggleTimeMenuButton(true);
                timePickerSettings.setInitialTimeToNow();
                timePickerSettings.setFormatForDisplayTime(PickerUtilities.createFormatterFromPatternString("HH:mm:ss", timePickerSettings.getLocale()));
                timePickerSettings.setFormatForMenuTimes(PickerUtilities.createFormatterFromPatternString("HH:mm:ss", timePickerSettings.getLocale()));
                int choice = JOptionPane.CANCEL_OPTION;
                try {
                    choice = JOptionPane.showConfirmDialog(frame, dateTimePicker, JDM.getText("Choose Time To Start"), JOptionPane.OK_CANCEL_OPTION);
                } catch (Exception e1) {
                    System.err.println("Unexpected Situation with Schedule Dialogue");
                }
                if (choice == JOptionPane.OK_OPTION) {
                    Date chosenDate = new Date(1000 * dateTimePicker.datePicker.getDate().toEpochSecond(dateTimePicker.timePicker.getTime(), OffsetDateTime.now().getOffset()));
                    scheduleManager.addSchedule(selectedDownload, chosenDate);
                }
            }
        });
        popupMenu.add(addToSchedule);

        popupMenu.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                int rowAtPoint = downloadTable.rowAtPoint(downloadTable.getMousePosition());
                if (rowAtPoint >= 0 && rowAtPoint < downloadTable.getRowCount()) {
                    downloadTable.clearSelection();
                    downloadTable.setRowSelectionInterval(rowAtPoint, rowAtPoint);
                } else {
                    downloadTable.clearSelection();
                }
                addToQueue.removeAll();
                JMenuItem item;
                for (String queue : queueManager.getQueues()) {
                    item = new JMenuItem(queue);
                    item.addActionListener(e1 -> queueManager.addDownload(queue, downloadManager.get(downloadTable.convertRowIndexToModel(downloadTable.getSelectedRow()))));
                    addToQueue.add(item);
                }
                refreshTable();
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });

        downloadTable.setComponentPopupMenu(popupMenu);


        downloadTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2)
                    downloadManager.openDownload(downloadManager.get(downloadTable.convertRowIndexToModel(downloadTable.rowAtPoint(e.getPoint()))));
                refreshTable();
            }
        });

        languageRefresher.add(() -> {
            addToQueue.setText(JDM.getText("Add to Queue"));
            showInfo.setText(JDM.getText("Show Info"));
            addToSchedule.setText(JDM.getText("Schedule"));
        });
    }

    /**
     * Scheduler button
     */
    private void setScheduler() {
        JButton scheduleButton = new JButton();
        scheduleButton.setOpaque(false);
        scheduleButton.setForeground(Color.WHITE);
        scheduleButton.setHorizontalTextPosition(SwingConstants.CENTER);
        scheduleButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        ImageIcon imageIcon = null;
        try {
            imageIcon = new ImageIcon(ImageIO.read(new File("./resources/ScheduleIcon.png")));
            scheduleButton.setIcon(imageIcon);
        } catch (Exception e) {
            System.err.println("Couldn't load Schedule Icon !");
        }
        ScheduleGUI scheduleGUI = new ScheduleGUI(scheduleManager, frame, (imageIcon == null) ? null : imageIcon.getImage());
        scheduleButton.setText(JDM.getText("Scheduler"));
        scheduleButton.setToolTipText(JDM.getText("Access Schedule Menu"));
        sideToolBar.add(scheduleButton);

        scheduleButton.addActionListener(e -> scheduleGUI.show());

        languageRefresher.add(() -> {
            scheduleButton.setText(JDM.getText("Scheduler"));
            scheduleButton.setToolTipText(JDM.getText("Access Schedule Menu"));
        });

    }

    /**
     * Search Button
     */
    private void setSearch() {
        JButton searchButton = new JButton();
        searchButton.setOpaque(false);
        searchButton.setForeground(Color.WHITE);
        searchButton.setHorizontalTextPosition(SwingConstants.CENTER);
        searchButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        searchButton.setText(JDM.getText("Search"));
        searchButton.setToolTipText(JDM.getText("Search in Download List Fie name and URL"));
        try {
            searchButton.setIcon(new ImageIcon(ImageIO.read(new File("./resources/DSearchIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Search Icon !");
        }
        searchButton.addActionListener(e -> {
            downloadTable.clearSelection();
            downloadTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            String searchQuery = null;
            try {
                searchQuery = ((String) JOptionPane.showInputDialog(frame, "Enter Search query", "Search", JOptionPane.QUESTION_MESSAGE,
                        new ImageIcon(ImageIO.read(new File("./resources/SearchIcon.png"))), null, "Search for .."));
            } catch (IOException e1) {
                System.err.println(e1.getMessage());
            }
            if (searchQuery == null || searchQuery.isEmpty())
                return;
            int index;
            Download download;
            for (int i = 0; i < downloadManager.get().size(); i++) {
                download = downloadManager.get(i);
                if (download.getUrl().toString().matches(".*" + searchQuery + ".*") || download.getFileName().matches(".*" + searchQuery + ".*")) {
                    index = downloadTable.convertRowIndexToView(i);
                    downloadTable.addRowSelectionInterval(index, index);
                }
            }

        });

        topToolBar.add(searchButton);

        languageRefresher.add(() -> {
            searchButton.setToolTipText(JDM.getText("Search in Download List Fie name and URL"));
            searchButton.setText(JDM.getText("Search"));
        });

    }

    /**
     * Queue manager icon and menu item
     */
    private void setQueueManager() {
        QueueManagerGUI queueManagerGUI = new QueueManagerGUI(queueManager, frame);
        JButton queueButton = new JButton();
        queueButton.setOpaque(false);
        queueButton.setForeground(Color.WHITE);
        queueButton.setHorizontalTextPosition(SwingConstants.CENTER);
        queueButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        try {
            queueButton.setIcon(new ImageIcon(ImageIO.read(new File("./resources/QueueIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Queue Icon !");
        }
        queueButton.setText(JDM.getText("Queues"));
        queueButton.setToolTipText(JDM.getText("Access Queues Menu"));
        sideToolBar.add(queueButton);


        queueButton.addActionListener(e -> queueManagerGUI.show());

        languageRefresher.add(() -> {
            queueButton.setText(JDM.getText("Queues"));
            queueButton.setToolTipText(JDM.getText("Access Queues Menu"));
        });
    }

    /**
     * Setting icon and menu
     */
    private void setSettings() {

        settingsGUI = new SettingsGUI(downloadManager, frame);
        JButton settingButton = new JButton();
        settingButton.setOpaque(false);
        settingButton.setForeground(Color.WHITE);
        settingButton.setHorizontalTextPosition(SwingConstants.CENTER);
        settingButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        try {
            settingButton.setIcon(new ImageIcon(ImageIO.read(new File("./resources/SettingIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Setting Icon !");
        }
        settingButton.setText(JDM.getText("Settings"));
        settingButton.setToolTipText(JDM.getText("Access Setting Menu"));
        sideToolBar.add(settingButton);


        ActionListener actionListener = e -> settingsGUI.showFrame();
        settingButton.addActionListener(actionListener);
        menuBar.getMenu(0).getItem(1).addActionListener(actionListener);

        languageRefresher.add(() -> {
            settingButton.setText(JDM.getText("Settings"));
            settingButton.setToolTipText(JDM.getText("Access Setting Menu"));
        });

    }

    /**
     * Remove icon and menu
     */
    private void setRemove() {
        JButton removeButton = new JButton();
        removeButton.setOpaque(false);
        removeButton.setForeground(Color.WHITE);
        removeButton.setHorizontalTextPosition(SwingConstants.CENTER);
        removeButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        try {
            removeButton.setIcon(new ImageIcon(ImageIO.read(new File("./resources/DeleteIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Remove Icon !");
        }
        removeButton.setText(JDM.getText("Remove"));
        removeButton.setToolTipText(JDM.getText("Removes selected download from list"));
        topToolBar.add(removeButton);
        ActionListener actionListener = e -> {
            try {
                int removeIndex = downloadTable.convertRowIndexToModel(downloadTable.getSelectedRow());
                downloadManager.removeDownload(downloadManager.get(removeIndex));
            } catch (IndexOutOfBoundsException ex) {
                System.err.println("No item Selected");
            }
            refreshTable();
        };
        removeButton.addActionListener(actionListener);
        downloadTable.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                    actionListener.actionPerformed(null);
                }
            }
        });
        menuBar.getMenu(1).getItem(3).addActionListener(actionListener);

        languageRefresher.add(() -> {
            removeButton.setText(JDM.getText("Remove"));
            removeButton.setToolTipText(JDM.getText("Removes selected download from list"));
        });
    }

    /**
     * Cancel button and menu
     */
    private void setCancel() {
        JButton cancelButton = new JButton();
        cancelButton.setOpaque(false);
        cancelButton.setForeground(Color.WHITE);
        cancelButton.setHorizontalTextPosition(SwingConstants.CENTER);
        cancelButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        try {
            cancelButton.setIcon(new ImageIcon(ImageIO.read(new File("./resources/CancelIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Cancel Icon !");
        }

        topToolBar.add(cancelButton);

        ActionListener actionListener = e -> {
            try {
                downloadManager.cancelDownload(downloadManager.get(downloadTable.convertRowIndexToModel(downloadTable.getSelectedRow())));
            } catch (IndexOutOfBoundsException ex) {
                System.err.println("No item Selected");
            }
            refreshTable();
        };
        cancelButton.addActionListener(actionListener);
        menuBar.getMenu(1).getItem(2).addActionListener(actionListener);

        languageRefresher.add(() -> {
            cancelButton.setText(JDM.getText("Cancel"));
            cancelButton.setToolTipText(JDM.getText("Cancels a Download which Cant't be resumed"));
        });
    }

    /**
     * pause button and menu
     */
    private void setPause() {
        JButton pauseButton = new JButton();
        pauseButton.setOpaque(false);
        pauseButton.setForeground(Color.WHITE);
        pauseButton.setHorizontalTextPosition(SwingConstants.CENTER);
        pauseButton.setVerticalTextPosition(SwingConstants.BOTTOM);

        try {
            pauseButton.setIcon(new ImageIcon(ImageIO.read(new File("./resources/PauseIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Pause Icon !");
        }
        pauseButton.setText(JDM.getText("Pause"));
        pauseButton.setToolTipText(JDM.getText("Pauses a Download"));
        topToolBar.add(pauseButton);

        ActionListener actionListener = e -> {
            try {
                downloadManager.pauseDownload(downloadManager.get(downloadTable.convertRowIndexToModel(downloadTable.getSelectedRow())));
            } catch (IndexOutOfBoundsException ex) {
                System.err.println("No item Selected");
            }
        };
        pauseButton.addActionListener(actionListener);
        menuBar.getMenu(1).getItem(1).addActionListener(actionListener);

        languageRefresher.add(() -> {
            pauseButton.setText(JDM.getText("Pause"));
            pauseButton.setToolTipText(JDM.getText("Pauses a Download"));
        });

    }

    /**
     * new download icon and menu
     */
    private void setNewDownload() {

        JButton newDownloadButton = new JButton();
        ImageIcon imageIcon = null;
        try {
            imageIcon = new ImageIcon(ImageIO.read(new File("./resources/AddIcon.png")));
            newDownloadButton.setIcon(imageIcon);
        } catch (Exception e) {
            System.err.println("Couldn't load Add Icon !");
        }
        NewDownloadWindow window = new NewDownloadWindow(downloadManager, queueManager, scheduleManager, (imageIcon == null ? null : imageIcon.getImage()));
        window.setLocationRelativeTo(frame);
        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                frame.setEnabled(true);
                frame.setFocusable(true);
                frame.requestFocus();
            }

            @Override
            public void windowActivated(WindowEvent e) {
                frame.setEnabled(false);
                frame.setFocusable(false);
            }
        });


        newDownloadButton.setOpaque(false);
        newDownloadButton.setForeground(Color.WHITE);
        newDownloadButton.setHorizontalTextPosition(SwingConstants.CENTER);
        newDownloadButton.setVerticalTextPosition(SwingConstants.BOTTOM);


        newDownloadButton.setText(JDM.getText("Add URL"));
        newDownloadButton.setToolTipText(JDM.getText("Add new address to download"));

        topToolBar.add(newDownloadButton);

        ActionListener actionListener = e -> window.setVisible(true);
        newDownloadButton.addActionListener(actionListener);
        menuBar.getMenu(0).getItem(0).addActionListener(actionListener);

        languageRefresher.add(() -> {
            newDownloadButton.setText(JDM.getText("Add URL"));
            newDownloadButton.setToolTipText(JDM.getText("Add new address to download"));
        });

    }

    /**
     * resume button and menu
     */
    private void setResume() {
        JButton resumeButton = new JButton();
        resumeButton.setOpaque(false);
        resumeButton.setForeground(Color.WHITE);
        resumeButton.setHorizontalTextPosition(SwingConstants.CENTER);
        resumeButton.setVerticalTextPosition(SwingConstants.BOTTOM);

        try {
            resumeButton.setIcon(new ImageIcon(ImageIO.read(new File("./resources/ResumeIcon.png"))));
        } catch (Exception e) {
            System.err.println("Couldn't load Resume Icon !");
        }
        resumeButton.setText(JDM.getText("Resume"));
        resumeButton.setToolTipText(JDM.getText("Resume a Download or Start it"));

        topToolBar.add(resumeButton);

        ActionListener actionListener = e -> {
            try {
                Download download = downloadManager.get(downloadTable.convertRowIndexToModel(downloadTable.getSelectedRow()));
                downloadManager.resumeDownload(download);

            } catch (IndexOutOfBoundsException ex) {
                System.err.println("No item Selected");
            }
            refreshTable();
        };
        resumeButton.addActionListener(actionListener);
        menuBar.getMenu(1).getItem(0).addActionListener(actionListener);

        languageRefresher.add(() -> {
            resumeButton.setText(JDM.getText("Resume"));
            resumeButton.setToolTipText(JDM.getText("Resume a Download or Start it"));
        });

    }

    /**
     * Costume model for showing downloads
     */
    private class DownloadTableModel extends AbstractTableModel {
        private static final int COLUMN_FILENAME = 0;
        private static final int COLUMN_SIZE = 1;
        private static final int COLUMN_PROGRESS = 2;
        private static final int COLUMN_TIME_ADDED = 3;
        private static final int COLUMN_SPEED = 4;
        private static final int COLUMN_URL = 5;

        private String[] columnNames = {("Filename"), ("Size"), ("Progress"), ("Time Added"), ("Speed"), ("URL")};

        DownloadTableModel() {
            super();
        }

        @Override
        public int getRowCount() {
            return downloadManager.get().size();
        }

        @Override
        public int getColumnCount() {
            return columnNames.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex > 5) {
                throw new IllegalArgumentException("Illegal Column Index");
            }
            try {
                Download download = downloadManager.get(rowIndex);
                switch (columnIndex) {
                    case COLUMN_FILENAME:
                        return download.getFileName();
                    case COLUMN_PROGRESS:
                        return download.getProgress();
                    case COLUMN_SIZE:
                        return BigDecimal.valueOf(download.getSize() * 1e-3).setScale(2, RoundingMode.HALF_UP).doubleValue() + " KB";
                    case COLUMN_TIME_ADDED:
                        return download.getStartTime();
                    case COLUMN_SPEED:
                        return download.getRate() + " KB/s";
                    case COLUMN_URL:
                        return download.getUrl();
                    default:
                        throw new IllegalArgumentException("Illegal Column Index");
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.err.println("Column " + columnIndex + " Row " + rowIndex + "is illegal");
                return null;
            }

        }

        @Override
        public String getColumnName(int column) {
            return columnNames[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return downloadManager.get().isEmpty() ? Object.class : getValueAt(0, columnIndex).getClass();
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        /**
         * Getting name of a column
         *
         * @param index index of column
         * @return the name of it
         */
        String getText(int index) {
            return columnNames[index];
        }

    }

    /**
     * Renderer for progress bar in table
     */
    private class ProgressBarRenderer extends JProgressBar implements TableCellRenderer {
        ProgressBarRenderer() {
            super(JProgressBar.HORIZONTAL);
            setBorderPainted(false);
            setStringPainted(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (value instanceof Double) {
                setBackground(table.getBackground());
                double i = ((Double) value);
                setValue((int) i);
            }
            return this;
        }

    }


}


