package jdm;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Manager foe queue
 * Does operations like starting a queue , creating queue , removing a queue
 */
public class QueueManager implements Serializable {

    private Vector<Queue> queues;
    private DownloadManager downloadManager;

    /**
     * Initializes fields
     *
     * @param downloadManager the download manager used
     */
    QueueManager(DownloadManager downloadManager) {
        this.downloadManager = downloadManager;
        queues = new Vector<>();
    }

    /**
     * Creates a queue
     *
     * @param name name of queue
     * @throws IllegalCallerException when a queue with same input name already exists
     */
    public void createQueue(String name) throws IllegalCallerException {
        Queue queue = new Queue(name, downloadManager);
        if (queues.contains(queue))
            throw new IllegalCallerException("Queue with same name already Exists !");
        else
            queues.add(queue);
    }

    /**
     * Removes a queue
     *
     * @param name removed queue
     */
    public void removeQueue(String name) {
        queues.remove(new Queue(name, downloadManager));
    }

    /**
     * Finds a queue
     *
     * @param name name of queue
     * @return found queue and {@code null} if not found
     */
    private Queue findQueue(String name) {
        Queue queue = new Queue(name, downloadManager);
        if (queues.contains(queue))
            return queues.get(queues.indexOf(queue));
        else {
            return null;
        }
    }

    /**
     * Adding a download
     *
     * @param queueName name of queue to be added
     * @param download  added download
     */
    public void addDownload(String queueName, Download download) {
        //Searching in other queues :
        for (Queue q : queues)
            if (q.contains(download))
                q.remove(download);

        getQueues(queueName).addDownload(download);
    }

    /**
     * Removes a download from all queues
     *
     * @param download removed download
     */
    void removeDownload(Download download) {
        for (Queue queue : queues) {
            if (download.isDownloading())
                downloadManager.pauseDownload(download);
            queue.remove(download);
        }
    }

    //Getters:

    public String[] getQueues() {
        ArrayList<String> names = new ArrayList<>();
        for (Queue queue : queues)
            names.add(queue.toString());
        String[] namesArray = new String[names.size()];
        for (int i = 0; i < namesArray.length; i++) {
            namesArray[i] = names.get(i);
        }
        return namesArray;
    }

    public Queue getQueues(String name) {
        return findQueue(name);
    }

    public Vector<Queue> getQueuesList() {
        return queues;
    }

    public DownloadManager getDownloadManager() {
        return downloadManager;
    }

    void init(DownloadManager downloadManager) {
        this.downloadManager = downloadManager;
        for (Queue queue : queues)
            queue.init(downloadManager);
    }

    void exit() {
        for (Queue queue : queues)
            queue.stopDownloading();
    }

}
