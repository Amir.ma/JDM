package jdm;

import jdm.GUI.GUI;
import jdm.GUI.QueueManagerGUI;

import javax.net.ssl.*;
import javax.swing.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

/**
 * Downloading thread called by Event Dispatch Thread
 * Downloads and updates rate and progress of download
 */
class DownloadWorker extends SwingWorker<String, Long> {

    private static final int BUFFER_SIZE = 1024 * 8;
    private final Thread rateCalculator;
    private Download download;
    private URL url;
    private SegmentDownloader[] segmentDownloaderArray;
    private HttpURLConnection httpURLConnection = null;
    private HttpsURLConnection httpsURLConnection = null;

    /**
     * Initializes rate calculator and other fields
     *
     * @param download going to download it
     */
    DownloadWorker(Download download) {
        super();
        this.download = download;
        rateCalculator = new Thread(new RateCalculatorThread());
    }

    /**
     * Main Downloading threads which will handle segment downloading threads
     *
     * @return the state of downloading
     */
    @Override
    protected String doInBackground() {

        if (download.isCompleted())
            return "Already Downloaded";


        URLConnection connection;
        url = download.getUrl();

        //Change File name based on connection
        String preFileName = download.getFileName();
        try {
            connection = url.openConnection();
            String raw = connection.getHeaderField("Content-Disposition");
            //Getting Size of Content
            if (download.getSize() == 0) {
                download.setSize(connection.getContentLengthLong());
            }
            if (raw != null && raw.contains("="))
                download.setFileName(raw.split("=")[1].replaceAll("\"", "")); //getting value after '='
            else
                download.setFileName(preFileName);
        } catch (IOException e) {
            download.setFileName(preFileName);
        }

        String fileLocation = download.getFileLocation() + download.getFileName();
        File mainFile = new File(fileLocation);
        if (!mainFile.exists() || download.getDownloadedSize() == 0) {
            try {
                while (!mainFile.createNewFile()) {
                    //Changing file name to avoid replacement
                    System.out.println("File with same name Exists ! Renamed Automatically");
                    download.setFileName(new Random().nextInt(1000) + download.getFileName());
                    mainFile = new File(download.getFileLocation() + download.getFileName());
                }
            } catch (IOException e) {
                System.err.println(e.getMessage());
                return "IO Exception Failure";
            }
        }

        download.resetDownload();//Refresh progress and rate

        //Final Location of downloaded file
        fileLocation = download.getFileLocation() + download.getFileName();

        //Location of Segments :
        String[] segmentsLocations = new String[download.getNumberOfSegments()];
        for (int i = 0; i < segmentsLocations.length; i++) {
            segmentsLocations[i] = fileLocation + ".part" + i;
        }


        /*//Debugging stuff
        String destinationName = mainFile.getName();
        System.out.println("Destination Name = "+destinationName);
        download.setFileName(destinationName);
        System.out.println("File Name Chained to = " + download.getFileName());
        System.out.println("Save Path = " + mainFile.getPath());*/


        segmentDownloaderArray = new SegmentDownloader[download.getNumberOfSegments()];
        try {
            //Scope Variables used in loop
            File segmentFile;
            int responseCode;
            long max;
            long min;
            SegmentDownloader segmentDownloader;
            for (int i = 0; i < segmentsLocations.length; i++) {
                //If job is cancelled
                if (Thread.interrupted())
                    return "Paused";

                //Creating a connection
                connection = getConnection();
                if (connection == null) {
                    return "Connection Error";
                }
                segmentFile = new File(segmentsLocations[i]);
                //Request Resume
                min = segmentFile.length() + (i * (download.getSize() / download.getNumberOfSegments()));
                max = (i == download.getNumberOfSegments() - 1) ? download.getSize() : (i + 1) * (download.getSize() / download.getNumberOfSegments());
                download.downloaded(((int) segmentFile.length()));//Adding segments sizes to downloaded

                connection.addRequestProperty("Range", "bytes=" + min + "-" + max);
                //Requesting
                responseCode = (httpsURLConnection == null) ? httpURLConnection.getResponseCode() : httpsURLConnection.getResponseCode();
                //Check if link is redirected
                if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP
                        || responseCode == HttpURLConnection.HTTP_MOVED_PERM
                        || responseCode == HttpURLConnection.HTTP_SEE_OTHER)
                    System.err.println("Redirected.Unexpected Behaviour might occur");
                //Check if Server Accepts Resuming
                if (download.getDownloadedSize() != 0 && responseCode != HttpURLConnection.HTTP_PARTIAL) {
                    System.err.println("Server Refused to resume :(");
                    System.out.println("Response Code = " + responseCode);
                    return "Couldn't Resume. Try Cancelling Download";
                }
                //Initializing Downloader Thread after Checking
                if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_PARTIAL) {
                    segmentDownloader = new SegmentDownloader(new BufferedInputStream(connection.getInputStream()), max - min, new BufferedOutputStream(new FileOutputStream(segmentFile, true)), "Segment-" + i);
                    segmentDownloaderArray[i] = segmentDownloader;
                } else {
                    System.err.println("Connection Error");
                    System.out.println("Response = " + responseCode);
                    return "Connection Error";
                }
            }
        } catch (IOException e) {
            System.err.println("IO exception " + e.getMessage());
            return "IO Error";
        }


        //Rate calculator starts to do its job
        rateCalculator.start();

        //Starting to download
        for (SegmentDownloader segmentDownloader : segmentDownloaderArray)
            segmentDownloader.start();

        //Wait for all segments to download
        try {
            for (SegmentDownloader segmentDownloader : segmentDownloaderArray)
                segmentDownloader.join();
        } catch (InterruptedException e) {
            terminateAll();
            return "Paused";
        }


        /*//Debugging Stuff
        System.out.println("Http(s) Headers");
        //get all headers
        Map<String, List<String>> map = connection.getHeaderFields();
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            System.out.println("Key : " + entry.getKey() +
                    " ,Value : " + entry.getValue());
        }*/

        //Stop Calculating Rate
        synchronized (rateCalculator) {
            rateCalculator.interrupt();
        }

        //Combining Segments
        synchronized (Thread.currentThread()) {
            File segmentFile;
            FileInputStream segmentInputStream = null;
            ReadableByteChannel inputChannel;

            try (FileChannel mainFileChannel = new FileOutputStream(mainFile).getChannel()) {
                for (String segmentLocation : segmentsLocations) {
                    segmentFile = new File(segmentLocation);
                    if (!segmentFile.exists()) {
                        if (segmentInputStream != null)
                            segmentInputStream.close();
                        return "Error While Saving";
                    }
                    segmentInputStream = new FileInputStream(segmentFile);
                    inputChannel = segmentInputStream.getChannel();
                    mainFileChannel.position(mainFileChannel.position() + mainFileChannel.transferFrom(inputChannel, mainFileChannel.position(), segmentFile.length()));
                    inputChannel.close();
                }
            } catch (IOException | NullPointerException e) {
                System.err.println("Error While Joining Segments " + e.getMessage());
                return "Error While Saving";
            }

            download.completed();
        }

        for (String segmentLocation : segmentsLocations)
            if (!new File(segmentLocation).delete()) {
                System.err.println("Couldn't Delete Segment File");
            }

        return "Done";
    }

    /**
     * Terminates all running threads if needed
     */
    @SuppressWarnings("SynchronizationOnLocalVariableOrMethodParameter")
    private void terminateAll() {
        if (!isCancelled()) {
            cancel(true);
        }

        //Terminate all threads if needed
        if (segmentDownloaderArray != null) {
            for (SegmentDownloader segmentDownloader : segmentDownloaderArray)
                if (segmentDownloader != null && segmentDownloader.isAlive())
                    synchronized (segmentDownloader) {
                        segmentDownloader.interrupt();
                    }
        }
        if (rateCalculator.isAlive())
            synchronized (rateCalculator) {
                rateCalculator.interrupt();
            }
    }

    /**
     * Creates a connection
     * Also Distinguishes between HTTP and HTTPS
     *
     * @return opened connection and {@code null} if not successful
     */
    private URLConnection getConnection() {
        URLConnection result;
        httpURLConnection = httpsURLConnection = null;

        try {
            switch (url.getProtocol().toLowerCase()) {
                case "http":
                    httpURLConnection = ((HttpURLConnection) url.openConnection());
                    httpURLConnection.setRequestMethod("GET");
                    result = httpURLConnection;
                    break;
                case "https":
                    // Create a trust manager that does not validate certificate chains
                    TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }

                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }

                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }
                    }};
                    // Install the all-trusting trust manager
                    SSLContext sc = SSLContext.getInstance("SSL");
                    sc.init(null, trustAllCerts, new java.security.SecureRandom());
                    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
                    // Create all-trusting host name verifier
                    HostnameVerifier allHostsValid = (hostname, session) -> true;
                    // Install the all-trusting host verifier
                    HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
                    httpsURLConnection = ((HttpsURLConnection) url.openConnection());
                    httpsURLConnection.setRequestMethod("GET");
                    result = httpsURLConnection;
                    break;
                default:
                    return null;
            }
        } catch (IOException | NoSuchAlgorithmException | KeyManagementException e) {
            System.err.println("Error While Creating Connection " + e.getMessage());
            return null;
        }
        //If connection lost , wait 2 seconds
        result.setConnectTimeout(5000);
        result.setReadTimeout(5000);

        return result;
    }

    /**
     * Updates the downloading table in GUI
     *
     * @param chunks the published stuff(not used)
     */
    @Override
    protected void process(List<Long> chunks) {
        GUI.refreshTable();
//        QueueManagerGUI.refreshTable();//Not Necessary
    }

    /**
     * After a download is finished , streams are closed
     * Result is echoed
     * GUI is refreshed again
     */
    @Override
    protected void done() {
        try {
            System.out.print(download.getFileName() + " ");
            System.out.println("Downloading Result = " + get());
            terminateAll();
        } catch (NullPointerException e) {
            System.err.println("Null = ");
            e.printStackTrace();
        } catch (InterruptedException e) {
            System.err.println("Interrupted = " + e.getMessage());
        } catch (ExecutionException e) {
            System.err.println("Exec error = " + e.getMessage());
        } catch (CancellationException e) {
            System.err.println("Paused");
            terminateAll();
        } finally {
            download.setDownloading(false);
            if ((httpURLConnection != null || httpsURLConnection != null)) {
                if (httpURLConnection != null)
                    httpURLConnection.disconnect();
                else
                    httpsURLConnection.disconnect();
            }
            QueueManagerGUI.refreshTable();
            GUI.refreshTable();
            setProgress(1);
            System.out.flush();
        }
    }

    /**
     * Thread for downloading a thread
     */
    private class SegmentDownloader extends Thread {

        private final BufferedInputStream inputStream;
        private final BufferedOutputStream outputStream;
        private final long length;

        private SegmentDownloader(BufferedInputStream inputStream, long length, BufferedOutputStream outputStream, String name) {
            super(name);
            this.inputStream = inputStream;
            this.outputStream = outputStream;
            this.length = length;
        }

        @Override
        public void run() {

            /*
            try (BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                 BufferedOutputStream outputStream = new BufferedOutputStream(file))
             */

            int count = 0;
            int readCount = 0;
            byte[] buffer = new byte[BUFFER_SIZE];

            //Starting to Download
            try (inputStream; outputStream) {

                //Downloading
                while ((readCount += count) < length && (count = inputStream.read(buffer, 0, Math.min((int) (length - readCount), BUFFER_SIZE - 1))) > 0) {
                    if (Thread.interrupted())
                        break;

                    try {
                        if (!isCancelled()) {
                            outputStream.write(buffer, 0, count);
                        }
                    } catch (IOException e) {
                        outputStream.flush();
                        outputStream.close();
                        inputStream.close();
                        break;
                    }
                    download.downloaded(count);
                    publish(download.getDownloadedSize());//Updating downloaded size
                }

                outputStream.flush();
                outputStream.close();
                inputStream.close();
            } catch (IOException e) {//Input Stream Catch
                //what to do ?
            }

            System.out.println(getName() + " Received " + readCount + " Bytes");

        }

    }

    /**
     * Calculates rate at constant time interval based on downloaded size of file
     */
    private class RateCalculatorThread implements Runnable {
        @Override
        public void run() {
            Double rate;
            long count;

            while (!Thread.interrupted()) {
                Thread.onSpinWait();
                count = download.getDownloadedSize();
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    break;
                }
                count = download.getDownloadedSize() - count;
                if (count == 0)
                    continue;
                rate = (((count + 0.0D) / (500D)));//Calculating Speed as KB per Second
                if (!rate.isInfinite())
                    rate = BigDecimal.valueOf(rate).setScale(2, RoundingMode.HALF_UP).doubleValue();
                download.setRate(rate);
            }
        }

    }


}
