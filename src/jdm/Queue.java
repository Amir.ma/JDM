package jdm;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Objects;
import java.util.Vector;

/**
 * Stimulates a Queue of Downloads
 * Provides Operations on Queues like adding , removing , starting , stopping
 * A name Identifies a Queue
 */


@SuppressWarnings({"SynchronizeOnNonFinalField", "SynchronizationOnLocalVariableOrMethodParameter"})//Optional

public class Queue implements Serializable {

    private static Thread downloadingThread;//Downloads files one by one
    private Vector<Download> downloads;
    private String name;
    private DownloadManager downloadManager;

    /**
     * Initializes the queue
     *
     * @param name            name of queue
     * @param downloadManager manager for download operations
     */
    Queue(String name, DownloadManager downloadManager) {
        this.name = name;
        this.downloadManager = downloadManager;
        downloads = new Vector<>();
    }

    /**
     * Adding a download to queue
     *
     * @param download going to be added
     */
    void addDownload(Download download) {
        try {
            if (!downloads.contains(download)) {
                downloads.add(download);
            }
        } catch (Exception e) {
            System.err.println("Couldn't Add Download to Queue.Probably Because it's in progress.Sorry :X");
        }
    }

    /**
     * Starting a queue
     * Starts Downloading files one by one
     */
    public void startDownloading() {
        if (downloadingThread != null && downloadingThread.isAlive())
            return;

        downloadingThread = new Thread(() -> {
            Download download = null;
            Iterator<Download> iterator;

            while (true) {
                iterator = downloads.iterator();
                if (downloads.contains(download))
                    try {
                        download = downloads.get(downloads.indexOf(download) + 1);
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println(name + " Finished");
                        break;
                    }
                else if (iterator.hasNext())
                    download = iterator.next();
                else {
                    System.out.println(name + " Finished");
                    break;
                }
                if (!download.isDownloading())
                    downloadManager.resumeDownload(download);

//                Thread.onSpinWait();//Optional if wanted to remove lock and use spin (not suggested)

                try {
                    Thread currentThread = Thread.currentThread();
                    synchronized (currentThread) {
                        currentThread.wait();
                    }
                } catch (InterruptedException e) {//For Stopping Queue
                    downloadManager.pauseDownload(download);
                    break;
                }
            }

        });

        downloadingThread.start();

    }

    /**
     * Stops a queue from downloading
     */
    public void stopDownloading() {
        if (downloadingThread == null)
            return;
        synchronized (downloadingThread) {
            downloadingThread.interrupt();
        }
        System.out.println(name + " Stopped");
    }

    /**
     * Clears the queue
     */
    public void remove() {
        synchronized (downloadingThread) {
            downloadingThread.interrupt();
        }
        downloads.removeAllElements();
    }

    /**
     * Removes download with index
     *
     * @param index index of removed download
     */
    public void remove(int index) {
        Download download = downloads.get(index);
        if (download.isDownloading())
            synchronized (downloadingThread) {
                downloadingThread.interrupt();
            }

        downloads.remove(index);
    }

    /**
     * Removes a download if exists
     *
     * @param download removed download
     */
    void remove(Download download) {
        if (download.isDownloading())
            synchronized (downloadingThread) {
                downloadingThread.interrupt();
            }

        downloads.remove(download);
    }

    /**
     * Removing Completed Downloads
     */
    public boolean removeCompleted() {
        boolean b = downloads.removeIf(Download::isCompleted);

        if (downloadingThread != null) {
            synchronized (downloadingThread) {
                downloadingThread.notify();
            }
        }

        return b;
    }

    /**
     * Moves a download one item upper in the list
     *
     * @param index moved download
     */
    public int moveUp(int index) {
        if (index != 0) {
            Download selected = downloads.get(index);
            Download tmp = downloads.get(index - 1);
            downloads.set(index - 1, selected);
            downloads.set(index, tmp);
            return index - 1;
        } else
            return index;
    }

    void init(DownloadManager downloadManager) {
        this.downloadManager = downloadManager;
        for (int i = 0; i < downloads.size(); i++) {
            downloads.setElementAt(downloadManager.get(downloadManager.get().indexOf(downloads.get(i))), i);
        }
    }

    /**
     * Moves a download one item downer in the list
     *
     * @param index moved download
     */
    public int moveDown(int index) {
        if (index != downloads.size() - 1) {
            Download selected = downloads.get(index);
            Download tmp = downloads.get(index + 1);
            downloads.set(index + 1, selected);
            downloads.set(index, tmp);
            return index + 1;
        } else
            return index;
    }

    /**
     * Checks if the download is in this queue
     *
     * @param download searched download
     * @return whether exists in download or not
     */
    boolean contains(Download download) {
        return downloads.contains(download);
    }

    /**
     * Only name of queue is checked
     *
     * @param o to be compared object
     * @return whether equal or not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Queue)) return false;
        Queue queue = (Queue) o;
        return Objects.equals(name, queue.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }

    public Vector<Download> getDownloads() {
        return downloads;
    }

}

