package jdm;

import jdm.GUI.GUI;

import javax.swing.*;
import java.io.*;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Java Download Manager
 * Simple download managing application
 * Key Features :
 * <p>Queueing and Scheduling</p>
 * <p>Saving Everything and Language can be Changed and Zip exporting is available </p>
 * <p>Can download indirect Links and Supports Multi-Segment Downloading</p>
 *
 * @author Am.Aghapoor
 * @version Phase III final
 */
public class JDM {

    public static String language;
    static ScheduleManager scheduleManager;
    static private DownloadManager downloadManager;
    static private QueueManager queueManager;
    private static ConcurrentHashMap<String, String> languageMap;

    /**
     * Runs Program
     *
     * @param args probable options (not supported )
     */
    public static void main(String[] args) {

        //Install L&F
        try {
            UIManager.installLookAndFeel(new UIManager.LookAndFeelInfo("JTattoo", "com.jtattoo.plaf.smart.SmartLookAndFeel"));
        } catch (Exception e) {
            System.err.println("Couldn't Install L&F");
        }

        //Initialize
        languageMap = new ConcurrentHashMap<>(75);
        language = "English";

        //Loading Previously stored data
        load();

        if (downloadManager != null) {
            downloadManager.init();
            language = downloadManager.getLanguageDM();
            loadLanguage(language);
        } else {
            downloadManager = new DownloadManager();
            queueManager = null;
            scheduleManager = null;
            language = downloadManager.getLanguageDM();
            loadLanguage(language);
        }
        if (queueManager == null)
            queueManager = new QueueManager(downloadManager);
        else
            queueManager.init(downloadManager);
        if (scheduleManager == null) {
            scheduleManager = new ScheduleManager(downloadManager);
        }
        downloadManager.setQueueManager(queueManager);
        GUI gui = new GUI(queueManager, downloadManager, scheduleManager);
        SwingUtilities.invokeLater(gui::showGUI);

    }

    /**
     * Loads previously saved data if available
     */
    private static void load() {
        File downloadManagerFile = new File("SavedData/Downloads.jdm");
        if (!downloadManagerFile.exists()) {
            System.out.println("No Saved Data Exists");
            return;
        }

        //Loading Download List
        try (ObjectInputStream objectOutputStream = new ObjectInputStream(new FileInputStream(downloadManagerFile))) {
            downloadManager = ((DownloadManager) objectOutputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Download Reader : " + e.getMessage());
        }

        //Loading Queues
        try (ObjectInputStream objectOutputStream = new ObjectInputStream(new FileInputStream("SavedData/Queues.jdm"))) {
            queueManager = ((QueueManager) objectOutputStream.readObject());
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Queue Reader : " + e.getMessage());
        }

        //Loading Schedules
        try (ObjectInputStream objectOutputStream = new ObjectInputStream(new FileInputStream("SavedData/Schedule.jdm"))) {
            Object readObject = objectOutputStream.readObject();
            if (readObject instanceof ConcurrentHashMap)
                scheduleManager = new ScheduleManager(downloadManager, ((ConcurrentHashMap) readObject));
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Schedule Reader : " + e.getMessage());
        }
    }

    /**
     * Backup a removed download
     *
     * @param removedDownload the removed download
     */
    static void downloadRemoved(Download removedDownload) {
        if (removedDownload == null)
            return;
        try (FileWriter fileWriter = new FileWriter("RemovedLinks.txt", true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             PrintWriter printWriter = new PrintWriter(bufferedWriter)) {
            printWriter.printf("FileName : %s URL : %s", removedDownload.getFileName(), removedDownload.getUrl());
            printWriter.println();
            printWriter.flush();
        } catch (IOException e) {
            System.err.println("Removed List Writer : " + e.getMessage());
        }
    }

    /**
     * Get changed text after language changed
     *
     * @param input the required text
     * @return translated text
     */
    public static String getText(String input) {
        return Objects.requireNonNullElse(languageMap.get(input), input);
    }

    /**
     * Loads the words from changed language file
     *
     * @param language the new language
     */
    public static void loadLanguage(String language) {
        if (language == null)
            return;
        System.out.println("Language Loaded " + language);
        if (language.equals("English")) {
            languageMap.clear();
            JDM.language = language;
            downloadManager.setLanguageDM(language);
            return;
        }

        String line;//Local Variable for read Line
        String engWord;//English Read Word
        String transWord;//TranslatedWord

        File languageFile = new File("Languages/" + language + ".txt");
        if (!languageFile.canRead()) {
            System.err.println("Couldn't Load Language File !");
            return;
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(languageFile))) {
            while (reader.ready()) {
                line = reader.readLine();
                engWord = line.substring(0, line.indexOf("="));
                transWord = line.substring(line.indexOf("=") + 1, line.length());
//                System.out.printf("Eng Word is %s Trans is %s\n",engWord,transWord);
                languageMap.put(engWord, transWord);
            }
        } catch (IOException | IndexOutOfBoundsException e) {
            System.err.println("Couldn't load language");
        }

        JDM.language = language;
        downloadManager.setLanguageDM(language);
    }

    /**
     * Saves data and terminates program if needed
     *
     * @param doExit whether to terminate program or not (just save)
     */
    public static void exitProgram(boolean doExit) {

        //Pausing all Downloads
        downloadManager.exit();

        if (new File("SavedData").mkdir())
            System.out.println("Directory for saving data created");

        //Writing Download List
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("SavedData/Downloads.jdm"))) {
            objectOutputStream.writeObject(downloadManager);
        } catch (IOException e) {
            System.err.println("Download Writer : " + e.getMessage());
            e.printStackTrace();
        }

        //Writing Queues
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("SavedData/Queues.jdm"))) {
            objectOutputStream.writeObject(queueManager);
        } catch (IOException e) {
            System.err.println("Queue Writer : " + e.getMessage());
        }

        //Writing Schedule
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("SavedData/Schedule.jdm"))) {
            objectOutputStream.writeObject(scheduleManager.getDownloadDateHashMap());
        } catch (Exception e) {
            System.err.println("Schedule Writer : " + e.getMessage());
        }
        //Exiting . . .
        if (doExit)
            System.exit(0);
    }

}
