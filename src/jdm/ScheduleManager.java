package jdm;

import jdm.GUI.GUI;

import javax.swing.*;
import java.io.Serializable;
import java.util.*;
import java.util.Timer;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Schedules downloads
 */
public class ScheduleManager implements Serializable {
    private DownloadManager downloadManager;
    private Vector<String> schedules;
    private ArrayList<Timer> timers;

    //For Saving Fields
    private ConcurrentHashMap<Download, Date> downloadDateHashMap;

    /**
     * Initializes fields
     *
     * @param downloadManager for managing downloads
     */
    ScheduleManager(DownloadManager downloadManager) {
        this.downloadManager = downloadManager;
        schedules = new Vector<>();
        timers = new ArrayList<>();
        downloadDateHashMap = new ConcurrentHashMap<>();
    }

    @SuppressWarnings("unchecked")
    ScheduleManager(DownloadManager downloadManager, ConcurrentHashMap downloadDateHashMap) {
        this.downloadManager = downloadManager;
        this.downloadDateHashMap = ((ConcurrentHashMap<Download, Date>) downloadDateHashMap);
        schedules = new Vector<>();
        timers = new ArrayList<>();

        for (Download download : this.downloadDateHashMap.keySet()) {
            Date date = this.downloadDateHashMap.get(download);
            download = downloadManager.get().elementAt(downloadManager.get().indexOf(download));
            System.out.println("Download = " + download.getFileName() + "Date = " + date);
            addSchedule(download, date);
        }

        downloadDateHashMap.clear();
    }

    /**
     * Adds a new schedule for a download
     *
     * @param download scheduled download
     * @param date     time to start downloading
     */
    public void addSchedule(Download download, Date date) {
        if (date.before(new Date())) {
            JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), JDM.getText(download.getFileName() + " Schedule Time is passed !\nSchedule Will be ignored"), JDM.getText(JDM.getText("Error")), JOptionPane.ERROR_MESSAGE);
            System.err.println("Invalid URL");
            removeSchedule(download);
            return;
        }
        removeSchedule(download);
        schedules.add(download.getFileName() + " At " + date);
        Timer timer = new Timer();
        timer.schedule(new DownloadStarter(download), date);
        timers.add(timer);

        downloadDateHashMap.put(download, date);
    }

    /**
     * Removes a schedule using it's name
     *
     * @param schedule removed schedule
     */
    public void removeSchedule(String schedule) {
        int index = schedules.indexOf(schedule);
        Timer timer = timers.get(index);
        schedules.remove(index);
        timer.cancel();
        timers.remove(index);

        schedule = schedule.replaceAll(".*At ", "");
        schedule = schedule.trim();
        try {
            Iterator<Download> iterator = downloadDateHashMap.keySet().iterator();
            while (iterator.hasNext()) {
                if (downloadDateHashMap.get(iterator.next()).toString().equals(schedule)) {
                    iterator.remove();
                }
            }
        } catch (Exception e) {
            System.err.println("Couldn't Remove from Schedule Hash");
        }

    }

    /**
     * Removes a schedule using it's download
     *
     * @param download removes schedule for this download
     */
    void removeSchedule(Download download) {
        for (String schedule : schedules)
            if (schedule.contains(download.getFileName()))
                schedules.set(schedule.indexOf(schedule), null);
        while (schedules.contains(null))
            schedules.remove(null);

        downloadDateHashMap.remove(download);
    }

    //Getters :
    public Vector<String> getSchedules() {
        return schedules;
    }

    ConcurrentHashMap<Download, Date> getDownloadDateHashMap() {
        return downloadDateHashMap;
    }

    /**
     * Starting a download Task
     */
    private class DownloadStarter extends TimerTask {
        private Download download;

        DownloadStarter(Download download) {
            this.download = download;
        }

        @Override
        public void run() {
            downloadManager.resumeDownload(download);
            GUI.refreshTable();
            removeSchedule(download);
        }

        @Override
        public boolean cancel() {
            downloadManager.cancelDownload(download);
            return true;
        }

    }

}
