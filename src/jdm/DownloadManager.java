package jdm;

import jdm.GUI.GUI;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Managing downloads
 * Operations like starting and pausing and creating downloads are done by this
 */
public class DownloadManager implements Serializable {
    public static final int UNLIMITED_NUMBER_OF_SIMULTANEOUS_DOWNLOADS = 29;
    private static ConcurrentHashMap<Download, DownloadWorker> downloadWorkerMap = new ConcurrentHashMap<>();
    private static ThreadPoolExecutor downloadsThreads;//Managing Maximum Number of Running Downloading
    private QueueManager queueManager;
    private Vector<Download> downloads;
    private int numberOfSimultaneousDownloads;
    private int numberOfSegments;
    private String defaultSaveFileLocation;
    private String lookAndFeel;
    private String languageDM;//Used to save language
    private Vector<String> forbiddenLinks;//Black list for download

    /**
     * Initializes the fields
     */
    DownloadManager() {
        numberOfSimultaneousDownloads = UNLIMITED_NUMBER_OF_SIMULTANEOUS_DOWNLOADS;
        downloads = new Vector<>();
        forbiddenLinks = new Vector<>();
        downloadsThreads = new ThreadPoolExecutor(numberOfSimultaneousDownloads, numberOfSimultaneousDownloads, 10, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(20));
        downloadsThreads.allowCoreThreadTimeOut(true);
        defaultSaveFileLocation = Paths.get(".").toAbsolutePath().normalize().toString();//Getting Running Path of Program
        numberOfSegments = 1;
    }

    /**
     * Initializing static fields if this is loaded from a file
     */
    void init() {
        downloadsThreads = new ThreadPoolExecutor(numberOfSimultaneousDownloads, numberOfSimultaneousDownloads, 10, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<>(20));
        downloadsThreads.allowCoreThreadTimeOut(true);
    }

    /**
     * Adding new download using url and save file location
     *
     * @param url          url of download
     * @param saveLocation saveLocation of download
     * @return created download
     */
    public Download newDownload(URL url, String saveLocation) {

        //Setting File Name and save location
        String fileName = url.getFile();

        if (fileName.isEmpty() || fileName.contains("\\") || fileName.matches(".*[?:*<>|\"].*"))
            fileName = "Download" + downloads.size();
        fileName = fileName.replaceAll("/", "");


        if (!saveLocation.endsWith("\\")) {
            if (saveLocation.contains("\\"))
                saveLocation = saveLocation + "\\";
            else
                saveLocation = saveLocation + "/";
        }


        //Size of Download is not set here
        Download toBeAdded = new Download(url, fileName, saveLocation, new Date(), numberOfSegments);

        if (!downloads.contains(toBeAdded))
            downloads.add(toBeAdded);
        else
            System.err.println("Download Already Exists");

        return toBeAdded;
    }

    /**
     * Resumes an download
     *
     * @param download to start (or resume)
     */
    public void resumeDownload(Download download) {

        if (!downloads.contains(download)) {
            System.err.println("Not In The List");
            return;
        }

        if (download.isDownloading()) {
            System.out.println("Download in Progress !");
            return;
        }

        if (isForbidden(download.getUrl().toString())) {
            //Better Not Be Here
            JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), JDM.getText("Download is Forbidden !"), JDM.getText("Warning"), JOptionPane.WARNING_MESSAGE);
            System.out.println("Download is Forbidden !");
            return;
        }

        download.setDownloading(true);

        DownloadWorker worker = new DownloadWorker(download);

        downloadWorkerMap.put(download, worker);

        try {
            downloadsThreads.execute(worker);
        } catch (Exception e) {
            System.err.println("Exception :)");
        }

    }

    /**
     * Cancels a download
     *
     * @param download cancelled download
     */
    public void cancelDownload(Download download) {
        File toDelete;
        System.out.flush();
        System.out.println(download.getFileName() + " Cancelled");

        try {
            //Check if download in progress
            if (download.isDownloading()) {
                DownloadWorker worker = downloadWorkerMap.get(download);
                worker.cancel(true);
                while (worker.getProgress() != 1) {
                    Thread.onSpinWait();
                }
            }

            //Waiting a bit
            Thread.sleep(100);

            //Removing Part Files if needed
            if (!download.isCompleted() && download.getSize() != 0 && download.getDownloadedSize() != 0) {
                for (int i = 0; i < download.getNumberOfSegments(); i++) {
                    toDelete = new File(download.getFileLocation() + download.getFileName() + ".part" + i);
                    if (toDelete.isDirectory()) {
                        System.err.println("Directory :/");
                        return;
                    }
                    if (!toDelete.delete()) {
                        System.err.println("Couldn't Delete Part File");
                    }
                }
            }

            //Removing Downloaded File
            if (download.getSize() != 0 && download.getDownloadedSize() != 0) {
                toDelete = new File(download.getFileLocation() + download.getFileName());
                if (toDelete.isDirectory()) {
                    System.err.println("Directory :/");
                    return;
                }
                if (!toDelete.delete()) {
                    System.err.println("Couldn't Delete Downloaded File");
                }
            }
        } catch (NullPointerException e) {
            //Nothing
        } catch (InterruptedException e) {
            System.err.println("What the ... ?");
        }
        download.resetDownload();
        GUI.refreshTable();
    }

    /**
     * Pauses a download
     *
     * @param download paused download
     */
    public void pauseDownload(Download download) {
        if (download.isDownloading()) {
            downloadWorkerMap.get(download).cancel(true);
        }
    }

    /**
     * Removes a download from list
     *
     * @param download remove download
     */
    public void removeDownload(Download download) {

        if (download.isDownloading())
            pauseDownload(download);

        if (queueManager != null) {
            queueManager.removeDownload(download);//Removing from queues
        }
        if (JDM.scheduleManager != null) {
            JDM.scheduleManager.removeSchedule(download);
        }

        if (!download.isCompleted())
            cancelDownload(download);

        downloads.remove(download);
        JDM.downloadRemoved(download);

        System.out.println("download = [" + download + "]" + " Removed");
    }

    /**
     * Opens the downloaded file
     *
     * @param download the opened download
     */
    public void openDownload(Download download) {
        if (download.isCompleted()) {
            File downloadedFile = new File(download.getFileLocation() + download.getFileName());
            if (downloadedFile.exists()) {
                try {
                    if (Desktop.isDesktopSupported())
                        Desktop.getDesktop().open(downloadedFile);
                    else
                        System.err.println("Couldn't Open :(");
                } catch (IOException e) {
                    System.err.println("Error while Opening = " + e.getMessage());
                }

            }
        } else {
            //Better Not be Here
            JOptionPane.showMessageDialog(JOptionPane.getRootFrame(), JDM.getText("File Not Downloaded Yet !"), JDM.getText("Warning"), JOptionPane.WARNING_MESSAGE);
            System.out.println("File not downloaded Yet !");
        }
    }

    /**
     * Adds a filter
     *
     * @param exception the exception url
     */
    public void addException(String exception) {
        exception = exception.trim();
        if (!forbiddenLinks.contains(exception))
            forbiddenLinks.add(exception);
    }

    /**
     * Removes an exception
     *
     * @param exception removed exception
     */
    public void removeException(String exception) {
        forbiddenLinks.remove(exception);
    }

    /**
     * Checks if a URL is in the forbidden links or not
     *
     * @param url the input url
     * @return whether it is in the black list or not
     */
    public boolean isForbidden(String url) {
        for (String forbiddenLink : forbiddenLinks) {
            if (url.contains(forbiddenLink))
                return true;
            if (url.matches(".*" + forbiddenLink + ".*"))
                return true;
            if (url.regionMatches(0, forbiddenLink, 0, forbiddenLink.length()))
                return true;
        }
        return false;
    }

    public String getDefaultSaveFileLocation() {
        return defaultSaveFileLocation;
    }

    public void setDefaultSaveFileLocation(String defaultSaveFileLocation) {
        this.defaultSaveFileLocation = defaultSaveFileLocation;
    }

    public Vector<Download> get() {
        return downloads;
    }

    public Download get(int index) {
        return downloads.get(index);
    }

    public String getLookAndFeel() {
        return lookAndFeel;
    }

    public void setLookAndFeel(String lookAndFeel) {
        this.lookAndFeel = lookAndFeel;
    }

    public int getNumberOfSimultaneousDownloads() {
        return numberOfSimultaneousDownloads;
    }

    //Setters & Getters :
    public void setNumberOfSimultaneousDownloads(int numberOfSimultaneousDownloads) {
        this.numberOfSimultaneousDownloads = numberOfSimultaneousDownloads;
        try {
            if (numberOfSimultaneousDownloads > downloadsThreads.getCorePoolSize()) {
                downloadsThreads.setMaximumPoolSize(this.numberOfSimultaneousDownloads);
                downloadsThreads.setCorePoolSize(this.numberOfSimultaneousDownloads);
            } else {
                downloadsThreads.setCorePoolSize(this.numberOfSimultaneousDownloads);
                downloadsThreads.setMaximumPoolSize(this.numberOfSimultaneousDownloads);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Vector<String> getForbiddenLinks() {
        return forbiddenLinks;
    }

    void setQueueManager(QueueManager queueManager) {
        this.queueManager = queueManager;
    }

    String getLanguageDM() {
        return languageDM;
    }

    void setLanguageDM(String languageDM) {
        this.languageDM = languageDM;
    }

    public int getNumberOfSegments() {
        return numberOfSegments;
    }

    public void setNumberOfSegments(int numberOfSegments) {
        this.numberOfSegments = numberOfSegments;
    }

    /**
     * Pauses are downloads
     */
    void exit() {
        downloadsThreads.shutdownNow();
        queueManager.exit();
    }

}
